<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdjustScore extends Model
{
    //
    protected $table="departments";
    protected $fillable = ['Snum','department','OragScore','StudentAve','TeacherAve','TeacherSta'
    ,'WantAve','WantSta','TeacherZ','AdjustScore','AdjustAve','ZRMax','ZRMin','dZR'
    ,'Level','Coefficient','PRGap','login'];
}
