<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Match extends Model
{
    //
    protected $table = "matches";
    protected $fillable = [
        'Snum', 'Tnum','choose','department','login'
    ];
    public static function del($department)
    {
        $match = DB::table('matches')->where('department', $department)->where('login', '2')->delete();
        return $match;
    }
}
