<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;
use App\Grade;
use App\StudentDepartment;
use App\Student;
use App\Teacher;
use App\Match;
use App\MatchNum;
use App\MarkData;
use \Cache;
use Illuminate\Support\Facades\Hash;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (session()->get('status') == "office") {
            return response(view('Department.match'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
        } else {
            setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
            $url = "https://sso.nuu.edu.tw/api/logout.php";
            $data_array = array("account" => session()->get('account'));
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data_array)
                )
            );
            $context  = stream_context_create($options);
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        
        
        if(session()->get('status')=="office"){
            $department = session()->get('department');
            // if(Teacher::where('department',$department)->whereIn('login', [ 2, 3])->doesnexist()){

            // }
            //下面是做json格式
            $num = MatchNum::where('department',$department)->whereIn('login', [ 2, 3])->value('number');
            $data['number'] = $num;
            $teacher = Teacher::where('department',$department)->whereIn('login', [ 2, 3])->select('Tname','Taccount')->get();
            $data['teacher'] = $teacher;
            if ($num != null) {//已有配對數
                // $teacher = Teacher::where('department',$department)->whereIn('login', [ 2, 3])->select('Tname','Taccount')->get();
                // $data['teacher'] = $teacher;
                $student=DB::table('student_departments')
                    ->join('students', 'student_departments.Snum', '=', 'students.Snum')
                    ->where('student_departments.login', 2)
                    ->where('student_departments.department',$department)
                    ->select('students.Snum', 'students.Sname')->get();
                $data['Student2'] = $student;

                $o = -1;
                $matcharray2=array();
                $teacher = Teacher::where('department',$department)->whereIn('login', [ 2, 3])->select('Tname','Taccount')->pluck('Taccount');
                foreach ($teacher as $e) {
                    $exist = Match::where('Taccount', $e)->where('department',$department)->whereIn('login', [ 2, 3])->where('choose', '1')->exists();
                    if ($exist) {
                        $studentarray2=DB::table('matches')
                            ->join('students', 'matches.Snum', '=', 'students.Snum')
                            ->where('matches.login', 2)
                            ->where('matches.Taccount',$e)
                            ->where('matches.department',$department)
                            ->select('students.Snum', 'students.Sname')->get();
                        $match2 = array();
                        $match2['TAccount'] = $e;
                        $match2['Snum'] = $studentarray2;
                        $matcharray2[++$o] = $match2;
                    }
                    // dd($e,Match::where('Taccount', $e)->get(),Match::pluck('Taccount'));
                }
                
                if (isset($matcharray2)) $data['SelectStudent'] = $matcharray2;

                //寫是否已評分
                if(Grade::where('department',$department)->whereIn('login', [ 2, 3])->where('score','!=' ,'0')->exists()){
                    $data['hasjudge'] = true;
                }else{
                    $data['hasjudge'] = false;
                }



            }
            return urldecode(json_encode($data));
        }
        //         }
        //     }
        // }

        return json_encode([]);
        //show的功能

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if (isset($request->Taccount) && isset($request->Snum)) {
            $department= session()->get('department');    
            Match::del($department);
            $student = Grade::del($department);
            $count = count($request->Taccount);
            $Taccount = $request->Taccount;
            $Snum = $request->Snum;
            $studentnotfull="";
            for ($b = 0; $b < $count; $b++) {
                $teachernum=0;
                $Snum1 = explode(",", $Snum[$b]);
                foreach ($Snum1 as $a) {
                    if( $a == null )continue;
                    $datas['Snum'] = $a;
                    $datas['Taccount'] = $Taccount[$b];
                    $datas['department'] = $department;
                    $datas['login'] = '2';
                    $datas['choose'] = '1';
                    Match::insert($datas);
                    $gbitem = MarkData::where('department', $department)->where('Sitem', '0')->where('login', '2')->pluck('id');
                    foreach ($gbitem as $h) {
                        $gdatas['Snum'] = $a;
                        $gdatas['Taccount'] = $Taccount[$b];
                        $gdatas['department'] = $department;
                        $gdatas['Bitem'] = DB::table('mark_data')->where('id', $h)->value('Bitem');
                        $gdatas['Mitem'] = DB::table('mark_data')->where('id', $h)->value('Mitem');                        
                        $gdatas['score'] =0;
                        $gdatas['block'] =-1;
                        $gdatas['login'] = 2;
                        Grade::insert($gdatas);
                    }                   
                }
            }
            // 
            // $checkS=;
            $trachernum=MatchNum::where('department', $department)->where('login', '2')->value('number');
            foreach( StudentDepartment::where('department',$department)->where('login', '2')->pluck('Snum') as $c ){
                if( Match::where('department',$department)->where('login', '2')->where('Snum',$c)->count() != $trachernum ){
                    $studentnotfull=$studentnotfull.(string)$a."  ";
                }
            }
            
            if($studentnotfull!=""){
                Cache::put('datastatus', $studentnotfull.'被分配的老師數不足' , 3);
               
            }else{
                Cache::put('datastatus', '更新成功' , 3);
                
            }
            return redirect()->route('match.index');
        }
        // dd('null');
        // return redirect()->route('match.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //更新配對人數  把舊的資料刪除，插入新的配對資料，並重設配對表
        //待test
        $department = session()->get('department');
        if (MatchNum::where('department', $department)->where('login', '2')->exists()) {
            MatchNum::where('department', $department)->where('login', '2')->delete();
        }
        $data['department'] = $department;
        $data['number'] = $request->number;
        $data['login']=2;
        MatchNum::insert($data);
        Match::where('department', $department)->where('login', '2')->delete();
        Cache::put('datastatus', '更新成功' , 3);
        return redirect()->route('match.index');
    }
}
