<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use App\DripEmailer;
use Illuminate\Console\Command;
use Artisan;

class admin extends Controller
{
    public function index()
    {
        //3
        if (session()->get('status')=="Admin") {
            return response(view('admin.index'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
        }
        switch (session()->get('status')) {
            case 'IR':
                return redirect('/student');
            case 'head':
                return redirect('/detail');
            case 'teacher':
                return redirect('/judge');
            case 'office':
                return redirect('/head');
            default:
                return  redirect('Undefined');
        }
        setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
        $url = "https://sso.nuu.edu.tw/api/logout.php";
        $data_array = array("account" => session()->get('account'));
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data_array)
            )
        );
        $context  = stream_context_create($options);
        session()->flush();
        session()->put('error', '權限不足 請重新登入');
        return view('welcome');
    }
    //
    public function store(Request $request)
    {
        $arrayId=["U0633126", "U0633117", "U0633010"];
        function getID($array,$id){
            foreach($array as $item){
                if($item==$id){
                    return true;
                }
            }
            return false;
        }
        if(!getID($arrayId,session()->get('account') )|| session()->get('status')!="Admin"){
            return redirect('/logout');
        }
        // $account = DB::table('flights')->where('id', '=', $request->Faccount)->get();
        // session()->flush();
        // dd($request->all());
        session()->put('Oldaccount',  session()->get('account'));
        session()->put('status',  $request->login);
        session()->put('account',  $request->Faccount);
        session()->put('name',  $request->Fname);
        session()->put('department',  $request->department);
        // session()->put('position',  $account[0]->position);
        $datetime = date ("Y-m-d H:i:s",mktime(date('H')+8, date('i')+15, date('s'), date('m'), date('d'), date('Y')));
        session()->put('availableTime', $datetime);
        return back();
    }
    public function show(Request $request)
    {
        //
        $keys = ["U0633126", "U0633117", "U0633010"];
        $allaccount = DB::table("flights")->select('Faccount', 'department', 'login', 'Fname')->distinct()->get();
        if ($request->cookie('key') != "") {
            // $request->cookie('key')
            foreach ($keys as $key => $value) {
                if (Hash::check($value, $request->cookie('key'))) {
                    // The passwords match...
                    if(isset($request->DB)){
                        switch ($request->DB) {
                            case '1':
                                $data=DB::table('flights')->get();
                                return json_encode($data);
                            case '2':
                                $data=DB::table('teachers')->get();
                                return json_encode($data);
                            case '3':
                                $data=DB::table('student_departments')
                                            ->join('students', 'student_departments.Snum', '=', 'students.Snum')
                                            ->select('students.*', 'student_departments.*')
                                            ->offset(isset($request->page)?$request->page*50:0)->limit(50)->get();
                                // $data=DB::table('students')->offset(isset($request->page)?$request->page*50:0)->limit(50)->get();                                
                                return json_encode($data);
                            case '4':
                                $data=DB::table('matches')->get();
                                return json_encode($data);
                            case '5':
                                $data=DB::table('match_nums')->get();
                                return json_encode($data);
                            case '6':
                                $data=DB::table('mark_data')->get();
                                return json_encode($data);
                            case '7':
                                $data=DB::table('grades')->get();
                                return json_encode($data);
                            case '8':
                                $data=DB::table('sessions')->get();
                                $data["sessions"]="123";
                                return json_encode($data);
                                
                        }
                    }
                    if(isset($request->gettotal)){
                        $data=DB::table('student_departments')->count();
                        return json_encode($data);
                    }
                    return json_encode($allaccount);
                }
            }
        }
        return json_encode([]);
    }
    public function update(Request $request)
    {
        Artisan::call('migrate:refresh --seed');
        return back();
    }
}
