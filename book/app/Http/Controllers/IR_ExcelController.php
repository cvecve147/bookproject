<?php

namespace App\Http\Controllers;

use App\Imports\UsersImport;
use DummyFullModelClass;
use App\IREcel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class IR_ExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\IREcel  $iREcel
     * @return \Illuminate\Http\Response
     */
    public function index(IREcel $iREcel)
    {
        //
    }
    public function updateflightsDepartOffice_FORIR(Request $request){
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx,csv'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(),$path);
        $check=1;

        if ($data->count() > 0){
            foreach ($data->toArray() as $ee){
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'Faccount' => str_replace(" ","",$row[0]),
                        'Fname' => $row[1],
                        'position' => "DepartOffice",
                        'department' => str_replace(" ","",$row[2]),
                        'login' => "office",
                    );
                }
            }
        }

        if(!empty($insert_data)){

            DB::table('flights')->where('login','=','office')->delete();

            DB::table('flights')->insert($insert_data);
        }

        return back()->with('success','Excel Data Imported successfully');
    }
    //以上是 update 系辦權限
}
