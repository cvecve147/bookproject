<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;
use phpDocumentor\Reflection\DocBlock\Tags\Method;

class loginTimeController extends Controller
{
    public function show(Request $request)
    {
        session()->start();
        $time1 = $request->session()->get('availableTime');
        $time2 = date ("Y-m-d H:i:s",mktime(date('H')+8, date('i'), date('s'), date('m'), date('d'), date('Y')));
        $result =(strtotime($time1) - strtotime($time2));
        if ($result <= 0) {
            return 0;
        }else{
            return json_encode($result);
        }
    }

    public function resetTime (Request $request){
        session()->start();
        /*$status = session()->get('status');
        $account = session()->get('account');
        $name = session()->get('name');
        $department = session()->get('department');
        $position = session()->get('position');
        session()->put('status',$status);
        session()->put('account',$account);
        session()->put('name',$name);
        session()->put('department',$department);
        session()->put('position',$position);*/
        $datetime = date ("Y-m-d H:i:s",mktime(date('H')+8, date('i')+15, date('s'), date('m'), date('d'), date('Y')));
        session()->put('availableTime',$datetime);
        return 9487;
    }
}
