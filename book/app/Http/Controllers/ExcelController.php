<?php

namespace App\Http\Controllers;

use App\Exports\exportOfRequestTimes;
use App\Exports\NowFlightsDataExport;
use App\Exports\OrderExport;
use App\Exports\InvoicesExport;
use App\Imports\UsersImport;
use Dotenv\Exception\ValidationException;
use DummyFullModelClass;
use App\Excelcontroll;
use http\Env\Response;
use http\Header;
use Illuminate\Database\QueryException;
use Illuminate\Filesystem\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\Event;
use Maatwebsite\Excel\Exceptions\LaravelExcelException;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Reader;
use Maatwebsite\Excel\Sheet;
use function Matrix\trace;
use mysql_xdevapi\Exception;
use mysql_xdevapi\Session;
use phpDocumentor\Reflection\Location;
use PhpOffice\PhpSpreadsheet\Worksheet\SheetView;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Excelcontroll $excelcontroll
     * @return \Illuminate\Http\Response
     */
    public function index(Excelcontroll $excelcontroll)
    {
        //
    }

    public function show(Request $request)
    {
        //dd(DB::table('mark_data')->select('*')->where('department', '=', "資訊管理學系")->get());
        //return Excel::download(new exportOfRequestTimes("經營管理學系", "109個人申請"), 'export_of_steps.xlsx');//測試用
        $verfy = DB::table('flights')->select('*')->where('Faccount', '=', session()->get('account'))->where('login', '=', 'IR')->get();
        if (count($verfy) > 0) {
            if (isset($request->department) and $request->times == "empityTimes") {
                cache()->put('datastatus', '請填寫管道資料或確認尺規資料有填寫正確!', 3);
                return back();
            } elseif (isset($request->department) and isset($request->times)) {
                if ($request->times == "@!") {
                    $request->times = " ";
                }

                $request->times = str_replace("@!", "", $request->times);
                $check = DB::table('grades')->select('Taccount')->where('department', '=', $request->department)->where('login', '=', $request->times)->distinct()->get();
                if (count($check) == 0) {
                    $check = DB::table('grades')->select('Taccount')->where('department', '=', $request->department)->where('login', '=', "2")->distinct()->get();
                    if (count($check) == 0) {
                        cache()->put('datastatus', '資料庫沒有資料!', 3);
                        return back();
                    } else
                        $result = DB::table('mark_data')->select('project')->where('department', '=', $request->department)->where('login', '=', '2')->distinct()->get();
                    return Excel::download(new OrderExport($request->department, "2", $request->department), $request->department . "_" . $result[0]->project . "_" . 'each_teacher_judged_record.xlsx');
                } else
                    $result = DB::table('mark_data')->select('project')->where('department', '=', $request->department)->where('login', '=', $request->times)->distinct()->get();
                return Excel::download(new OrderExport($request->department, $request->times, $request->department), $request->department . "_" . $result[0]->project . "_" . 'each_teacher_judged_record.xlsx');
            } elseif (isset($request->department)) {
                $data = DB::table('mark_data')->select('project', 'department')->where('department', '=', $request->department)->distinct()->get();

                return json_encode($data);
            } else {
                $data = DB::table('student_departments')->select('department', 'departmentid')->distinct()->get();
                return json_encode($data);
            }
        } else {
            echo "權限不足，請重新登入，重新導向...";
            if (isset($_COOKIE['token'])) {
                setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
                $url = "https://sso.nuu.edu.tw/api/logout.php";
                $data_array = array("account" => session()->get('account'));
                $options = array(
                    'http' => array(
                        'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method' => 'POST',
                        'content' => http_build_query($data_array)
                    )
                );
                $context = stream_context_create($options);
            }
            session()->flush();
            header("refresh:3 ; url=http://irmaterials.nuu.edu.tw/");
        }
    }

    public function expotexcel(Request $request)
    {
        if ($request->teacher == 1){
            return Excel::download(new OrderExport(1, 1,session()->get('department')), session('account').'_.judged_record.xlsx');
        }
        else if (count(DB::table('grades')->select('Taccount')->where('department', '=', session()->get('department'))->where('login', '=', '2')->distinct()->get()) > 0) {
            $result = DB::table('mark_data')->select('project')->where('department', '=', session()->get('department'))->where('login', '=', '2')->distinct()->get();
            return Excel::download(new OrderExport(0, 0, session()->get('department')), session()->get('department') . "_" . $result[0]->project . "_" . 'each_teacher_judged_record.xlsx');
        } else {
            cache()->put('datastatus', '資料庫沒有資料!', 3);
            return back();
        }
    }

    public function exportonlysteps(Request $request)
    {
        $result = DB::table('mark_data')->select('project')->where('department', '=', session()->get('department'))->where('login', '=', '2')->get();
        if ($result[0]->project == null or $result[0]->project == " ") {
            cache()->put('datastatus', '管道名稱錯誤:error資料庫沒有名稱，請先確認有無新增尺規', 3);
            return back();
        }else{
            return Excel::download(new exportOfRequestTimes(), session()->get('department') . '_' . $result[0]->project . '_' . '尺規匯出.xlsx');
        }
    }

    public function exportnewteacherdata()
    {
        $datetime = date("Y_m _d");
        return Excel::download(new NowFlightsDataExport(0), $datetime . '_書審系統_更新之老師資料(可直接重新匯入).xlsx');
    }

    public function exportnewheaddata()
    {
        $datetime = date("Y_m _d");
        return Excel::download(new NowFlightsDataExport(1), $datetime . '_書審系統_更新之主任資料(可直接重新匯入).xlsx');
    }

    public function exportnewofficedata()
    {
        $datetime = date("Y_m _d");
        return Excel::download(new NowFlightsDataExport(2), $datetime . '_書審系統_更新之系辦資料(可直接重新匯入).xlsx');
    }

    public function exportnewStudentdata()
    {
        $datetime = date("Y_m _d");
        return Excel::download(new NowFlightsDataExport(3), $datetime . '_書審系統_更新之學生資料(可直接重新匯入).xlsx');
    }

    public function importflightsDepartOffice(Request $request)
    {

        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx,csv'
        ]);

        $path = $request->file('select_file');
        $data = Excel::toCollection(new UsersImport(), $path);
        $check = 1;
        if ($data->count() > 0) {
            foreach ($data as $ee) {
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'Faccount' => str_replace(" ", "", $row[0]),
                        'Fname' => $row[1],
                        'position' => "DepartOffice",
                        'department' => str_replace(" ", "", $row[2]),
                        'login' => "office",
                    );
                }
            }
        }


        if (!empty($insert_data)) {
            try {
                DB::table('flights')->insert($insert_data);
                cache()->put('datastatus', '新增成功', 3);
                return back();
            } catch (QueryException $exception) {
                if ($exception->getCode() == "23000") {
                    cache()->put('datastatus', '資料庫無法接受重複資料！', 3);
                    return back();
                } else {
                    cache()->put('datastatus', '錯誤，錯誤代碼:' . $exception->getCode() . '，請聯絡維修人員', 3);
                    return back();
                }
            }

        }
    }

    //以上是insert 系辦權限 flights 表單
    public function importflightsDepartHead(Request $request)
    {
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx,csv'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(), $path);
        $check = 1;

        if ($data->count() > 0) {
            foreach ($data->toArray() as $ee) {
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'Faccount' => str_replace(" ", "", $row[0]),
                        'Fname' => $row[1],
                        'position' => 'head',
                        'department' => session()->get('department'),
                        'login' => "head",
                    );
                }
            }
        }
        //匯入
        if (!empty($insert_data)) {
            $vercheck = 1;
            foreach ($insert_data as $datatocheck) {
                $verification = DB::table('flights')->select('*')->where('Faccount', '=', $datatocheck['Faccount'])->where('department', '=', $datatocheck['department'])->get();
                //dd($verification);
                if (isset($verification[0])) {
                    $vercheck = 0;
                }
            }
            if ($vercheck == 0) {
                echo "...等待跳轉";
                cache()->put('datastatus', '請檢察excel檔案是否有「已新增過資料」...', 3);
                return back();
            } elseif ($vercheck == 1) {
                try {
                    DB::table('flights')->insert($insert_data);
                    cache()->put('datastatus', '新增成功', 3);
                    return back();
                } catch (QueryException $exception) {
                    if ($exception->getCode() == "23000") {
                        cache()->put('datastatus', '資料庫無法新增重複資料！', 3);
                        return back();
                    } else {
                        cache()->put('datastatus', '錯誤，錯誤代碼:' . $exception->getCode() . '，請聯絡維修人員', 3);
                        return back();
                    }
                }
            }

        }
    }

    //以上是 insert 主任權限
    public function updateflightsDepartHead(Request $request)
    {
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx,csv'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(), $path);
        $check = 1;

        if ($data->count() > 0) {
            foreach ($data->toArray() as $ee) {
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'Faccount' => str_replace(" ", "", $row[0]),
                        'Fname' => $row[1],
                        'position' => 'DepartHead',
                        'department' => session()->get('department'),
                        'login' => "head",
                    );
                }
            }
        }

        if (!empty($insert_data)) {

            foreach ($insert_data as $data) {
                try {
                    DB::table('flights')->where('login', $data['login'])->delete();
                } catch (QueryException $exception) {
                    if ($exception->getCode() == "23000") {
                        cache()->put('datastatus', '資料庫無法新增重複資料！', 3);
                        return back();
                    } else {
                        cache()->put('datastatus', '錯誤，錯誤代碼:' . $exception->getCode() . '，請聯絡維修人員', 3);
                        return back();
                    }
                }
            }

            try {
                DB::table('flights')->insert($insert_data);
                cache()->put('datastatus', '更新成功', 3);
                return back();
            } catch (QueryException $exception) {
                if ($exception->getCode() == "23000") {
                    cache()->put('datastatus', '資料庫無法新增重複資料！', 3);
                    return back();
                } else {
                    cache()->put('datastatus', '錯誤，錯誤代碼:' . $exception->getCode() . '，請聯絡維修人員', 3);
                    return back();
                }
            }
        }
    }

    //以上是 update 主任權限
    public function importstudents(Request $request)
    {
        try {
            $this->validate($request, [
                'select_file' => 'required|mimes:xls,xlsx,csv'
            ]);
        } catch (ValidationException $validationException) {
            dd($validationException);
        }

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(), $path);
        $check = 1;
        $checkForInsertdata = 1;
        $countForDuplicate = 0;
        if ($data->count() > 0) {
            foreach ($data->toArray() as $ee) {
                foreach ($ee as $row) {
                    if (count($row) == 12) {
                        if ($check >= 1) {
                            $check--;
                            continue;
                        }
                        if (isset($insert_data)) {
                            for ($x = 0; $x < $countForDuplicate; $x++) {
                                if ($insert_data[$x]['Snum'] == $row[3]) {
                                    $checkForInsertdata = 0;
                                }
                            }
                        }


                        $insert_data2[] = array(
                            'Snum' => $row[3],
                            'department' => str_replace(" ", "", str_replace("\f", "", $row[1])),
                            'departmentid' => str_replace(" ", "", str_replace("\f", "", $row[0])),
                            'login' => '2'
                        );
                        if ($checkForInsertdata == 1) {
                            $countForDuplicate += 1;
                            $insert_data[] = array(
                                'category' => $row[2],
                                'Snum' => $row[3],//有改格式
                                'Sname' => $row[4],
                                'gender' => $row[5],
                                'identifity' => $row[6],
                                'income' => $row[7],
                                'address' => $row[8],
                                'year' => $row[9],
                                'schoolnum' => $row[10],
                                'graduation' => $row[11],
                                'login' => "2"
                            );
                        } else {
                            $checkForInsertdata = 1;
                        }
                    } else {
                        cache()->put('datastatus', '格式不正確，請按照範例格式', 3);
                        return back();
                    }
                }
            }
        }
        if (!empty($insert_data) and !empty($insert_data2)) {
            try {
                DB::table('students')->insert($insert_data);
                DB::table('student_departments')->insert($insert_data2);
                cache()->put('datastatus', '新增成功', 3);
                return back();
            } catch (QueryException $e) {
                if ($e->getCode() == "23000") {
                    cache()->put('datastatus', '資料庫無法新增重複資料！', 3);
                    return back();
                } else {
                    cache()->put('datastatus', '錯誤，錯誤代碼:' . $e->getCode() . '，請聯絡維修人員', 3);
                    return back();
                }
            }
        }

    }

    //以上是匯入學生資料
    public function updatestudents(Request $request)
    {
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx,csv'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(), $path);
        $check = 1;
        $checkForInsertdata = 1;
        $countForDuplicate = 0;
        if ($data->count() > 0) {
            foreach ($data->toArray() as $ee) {
                foreach ($ee as $row) {
                    if (count($row) == 12) {
                        if ($check >= 1) {
                            $check--;
                            continue;
                        }

                        if (isset($insert_data)) {
                            for ($x = 0; $x < $countForDuplicate; $x++) {

                                if ($insert_data[$x]['Snum'] == $row[3]) {
                                    $checkForInsertdata = 0;
                                }
                            }
                        }
                        $insert_data2[] = array(
                            'Snum' => $row[3],
                            'department' => str_replace("\f", "", $row[1]),
                            'departmentid' => str_replace(" ", "", str_replace("\f", "", $row[0])),
                            'login' => '2'
                        );

                        if ($checkForInsertdata == 1) {
                            $countForDuplicate += 1;
                            $insert_data[] = array(
                                'category' => $row[2],
                                'Snum' => $row[3],//有改格式
                                'Sname' => $row[4],
                                'gender' => $row[5],
                                'identifity' => $row[6],
                                'income' => $row[7],
                                'address' => $row[8],
                                'year' => $row[9],
                                'schoolnum' => $row[10],
                                'graduation' => $row[11],
                                'login' => "2"
                            );
                        } else {
                            $checkForInsertdata = 1;
                        }
                    } else {
                        cache()->put('datastatus', '格式不正確，請按照範例格式', 3);
                        return back();
                    }
                }
            }
        }
        if (!empty($insert_data) and !empty($insert_data2)) {
            try {
                DB::table('students')->delete();
                DB::table('student_departments')->delete();
                DB::table('students')->insert($insert_data);
                DB::table('student_departments')->insert($insert_data2);
                cache()->put('datastatus', '更新成功', 3);
                return back();
            } catch (QueryException $e) {
                if ($e->getCode() == "23000") {
                    cache()->put('datastatus', '資料庫無法新增重複資料！', 3);
                    return back();
                } else {
                    cache()->put('datastatus', '錯誤，錯誤代碼:' . $e->getCode() . '，請聯絡維修人員', 3);
                    return back();
                }
            }
        }
    }

    //以上是更新學生資料
    public function importflightsDepartTeacher(Request $request)
    {
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx,csv'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(), $path);
        $check = 1;

        if ($data->count() > 0) {
            foreach ($data->toArray() as $ee) {
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'Faccount' => $row[0],
                        'Fname' => $row[1],
                        'position' => 'judge',
                        'department' => session()->get('department'),
                        'login' => "teacher",
                    );
                    $insert_data2[] = array(
                        'Tname' => $row[1],
                        'department' => session()->get('department'),
                        'Taccount' => $row[0],
                        'login' => "2",
                    );
                }
            }
        }

        if (!empty($insert_data) && !empty($insert_data2)) {
            $vercheck = 1;
            foreach ($insert_data as $datatocheck) {
                $verification = DB::table('teachers')->select('*')->where('Taccount', '=', $datatocheck['Faccount'])->where('department', '=', $datatocheck['department'])->where('login', '=', '2')->get();
                if (isset($verification[0])) {
                    $vercheck = 0;
                }
            }
            if ($vercheck == 0) {
                cache()->put('datastatus', '新增失敗，請檢察excel檔案是否有「已新增過資料」......', 3);
                return back();
            } elseif ($vercheck == 1) {
                try {
                    DB::table('flights')->insert($insert_data);
                    DB::table('teachers')->insert($insert_data2);
                    cache()->put('datastatus', '新增成功', 3);
                    return back();
                } catch (QueryException $e) {
                    if ($e->getCode() == "23000") {
                        cache()->put('datastatus', '資料庫無法接受重複資料！', 3);
                        return back();
                    } else {
                        cache()->put('datastatus', '錯誤，錯誤代碼:' . $e->getCode() . '，請聯絡維修人員', 3);
                        return back();
                    }
                }
            }
        }
    }

    //以上是給予老師權限
    public function updateflightsDepartTeacher(Request $request)
    {
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx,csv'
        ]);

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(), $path);
        $check = 1;

        if ($data->count() > 0) {
            foreach ($data->toArray() as $ee) {
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $insert_data[] = array(
                        'Faccount' => $row[0],
                        'Fname' => $row[1],
                        'position' => 'judge',
                        'department' => session()->get('department'),
                        'login' => "teacher",
                    );
                    $insert_data2[] = array(
                        'Tname' => $row[1],
                        'department' => session()->get('department'),
                        'Taccount' => $row[0],
                        'login' => "2",
                    );
                }
            }
        }

        if (!empty($insert_data) && !empty($insert_data2)) {
            foreach ($insert_data as $data) {
                try {
                    DB::table('flights')->where('department', '=', $data['department'])->where('login', '=', 'teachers')->delete();
                    DB::table('teachers')->where('department', '=', $data['department'])->delete();
                } catch (QueryException $exception) {
                    if ($exception->getCode() == "23000") {
                        cache()->put('datastatus', '資料庫無法接受重複資料！', 3);
                        return back();
                    } else {
                        cache()->put('datastatus', '錯誤，錯誤代碼:' . $exception->getCode() . '，請聯絡維修人員', 3);
                        return back();
                    }
                }
            }
            try {
                DB::table('flights')->insert($insert_data);
                DB::table('teachers')->insert($insert_data2);
                cache()->put('datastatus', '更新成功', 3);
                return back();
            } catch (QueryException $exception) {
                if ($exception->getCode() == "23000") {
                    cache()->put('datastatus', '資料庫無法接受重複資料！', 3);
                    return back();
                } else {
                    cache()->put('datastatus', '錯誤，錯誤代碼:' . $exception->getCode() . '，請聯絡維修人員', 3);
                    return back();
                }
            }
        }

    }

    //以上是更新老師之權限
    public function matchesImporter(Request $request)
    {
        try {
            $this->validate($request, [
                'select_file' => 'required|mimes:xls,xlsx,csv'
            ]);
        } catch (ValidationException $validationException) {
            dd($validationException);
        }

        $path = $request->file('select_file');

        $data = Excel::toCollection(new UsersImport(), $path);
        $check = 1;
        $teachernum = DB::table('match_nums')->select('number')->where('department', session()->get('department'))->where('login', '2')->get();
        $gbitem = DB::table('mark_data')->select('id')->where('Sitem', '0')->where('login', '2')->where('department', session()->get('department'))->get();
        $allstu = DB::table('student_departments')->select('Snum')->where('login', '2')->where('department', session()->get('department'))->get();
        $allteacher = DB::table('teachers')->select('Taccount')->where('department', session()->get('department'))->where('login', '2')->get();
        //dd($allstu,$allteacher);
        if ($data->count() > 0) {
            foreach ($data->toArray() as $ee) {
                foreach ($ee as $row) {
                    if ($check == 1) {
                        $check = 0;
                        continue;
                    }
                    $passtonext = 0;
                    for ($stunum = 0; $stunum < count($allstu); $stunum++) {
                        if ($row[0] == $allstu[$stunum]->Snum)
                            $passtonext = 1;
                    }
                    if ($passtonext == 0) {
                        cache()->put('datastatus', '沒有' . $row[0] . '此位學生，請重新確認', 3);
                        return back();
                    }
                    if (count($row) - 1 > $teachernum[0]->number or count($row) - 1 < $teachernum[0]->number) {
                        cache()->put('datastatus', '匯入檔案內容之老師配對超過或少於使用者輸入之老師數量!', 3);
                        return back();
                    }
                    for ($i = 0; $i <= $teachernum[0]->number; $i++) {
                        if ($row[$i] == null) {
                            cache()->put('datastatus', '匯入檔案內容之老師配對超過或少於使用者輸入之老師數量!', 3);
                            return back();
                        }
                    }
                    for ($xx = 1; $xx <= $teachernum[0]->number; $xx++) {
                        foreach ($gbitem as $a) {
                            $passtonext = 0;
                            for ($teanum = 0; $teanum < count($allteacher); $teanum++) {
                                if ($row[$xx] == $allteacher[$teanum]->Taccount) {
                                    $passtonext = 1;
                                }
                            }
                            if ($passtonext == 0) {
                                cache()->put('datastatus', '沒有' . $row[$xx] . '此位老師，請重新確認', 3);
                                return back();
                            }
                            $insert_data[] = array(
                                'Taccount' => $row[$xx],
                                'Snum' => $row[0],
                                'Bitem' => DB::table('mark_data')->where('id', $a->id)->value('Bitem'),
                                'Mitem' => DB::table('mark_data')->where('id', $a->id)->value('Mitem'),
                                'score' => 0,
                                'block' => -1,
                                'login' => 2,
                            );
                        }
                        $insert_data2[] = array(
                            'Snum' => $row[0],
                            'Taccount' => $row[$xx],//暫時
                            'department' => session()->get('department'),
                            'login' => '2',
                            'choose' => '1'
                        );
                    }
                }
            }
            try {
                DB::table('grades')->insert($insert_data);
                DB::table('matches')->insert($insert_data2);
                cache()->put('datastatus', '匯入配對成功!', 3);
                return back();
            } catch (QueryException $exception) {
                cache()->put('datastatus', '錯誤!，錯誤代碼:' . $exception->getCode() . "請洽維修人員", 3);
                return back();
            }
        }

    }//以上是匯入評分老師與學生配對

    public function stepsImporter(Request $request)
    {
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx,csv'
        ]);
        $path = $request->file('select_file');
        $data = Excel::toCollection(new UsersImport(), $path);
        $project = $path->getClientOriginalName();
        $project = explode("_",$project)[1];
        $Bitem = $data[0][3]->toArray();
        $Mitem = $data[0][0]->toArray();
        $Sitem = $data[0][1]->toArray();
        $range = $data[0][2]->toArray();
        $nowBitem = "空";
        $checkMatchData = DB::table('matches')->select('Snum','Taccount')->where('department','=',session()->get('department'))->where('choose','=',true)->where('login','=','2')->get();
        if (isset($checkMatchData))$checkMatch = true;
        else $checkMatch = false;
        if ($data->count() > 0) {
            for ($moveon = 0,$count=0,$moveonG=0,$Bitm=0,$Mitm = 0,$Sitm = 0;$moveon < count($Bitem);$moveon++,$count++){
                if ($Bitem[$moveon] != null) {
                    $Mitm=$Sitm=0;
                    $nowBitem = $Bitem[$moveon];
                    $str = explode(" ",$nowBitem);
                    $title = $str[0];
                    $percent = explode(":",end($str)); //拆字串
                    $percent = end($percent);
                    if ($checkMatch) {
                        for ($x = 0;$x<count($checkMatchData);$x++) {
                            $gradedata = array(
                                'department' => session()->get('department'),
                                'Snum' => $checkMatchData[$x]->Snum,
                                'Bitem' => "$Bitm",
                                'Mitem' => "$Mitm",
                                'Taccount' => $checkMatchData[$x]->Taccount,
                                'score' => 0,
                                'block' => -1,
                                'login' => "2"
                            );
                            $insert_data2[$moveonG++] = $gradedata;
                        }
                    }
                    $tempdata = array(
                        'department' => session()->get('department'),
                        'Bitem' => "$Bitm",
                        'Mitem' => "$Mitm",
                        'Sitem' => "$Sitm",
                        'title' => $title,
                        'HighScore' => null,
                        'LowScore' => null,
                        'login' => "2",
                        'percent' => (int)str_replace("%","",$percent),
                        'project' => $project
                    );
                    $Sitm++;
                    $insert_data[$count] = $tempdata;
                    $title = str_replace("\n", "<br>", str_replace("\t", "", $Sitem[$moveon]));
                    $high = explode("~", $range[$moveon])[0];
                    $low = explode("~", $range[$moveon])[1];
                    $tempdata = array(
                        'department' => session()->get('department'),
                        'Bitem' => (string)($Bitm),
                        'Mitem' => (string)($Mitm),
                        'Sitem' => "$Sitm",
                        'title' => $title,
                        'HighScore' => $high,
                        'LowScore' => $low,
                        'login' => "2",
                        'percent' => 0,
                        'project' => $project
                    );
                    $insert_data[++$count] = $tempdata;
                    $Bitm++;
                    $Mitm++;
                    $Sitm++;
                }else{
                    if ($Mitem[$moveon] != null){
                        $Sitm = 0;
                        $str = explode(" ",$nowBitem);
                        $percent = explode(":",end($str)); //拆字串
                        $percent = end($percent);
                        if ($checkMatch) {
                            for ($x = 0;$x<count($checkMatchData);$x++) {
                                $gradedata = array(
                                    'department' => session()->get('department'),
                                    'Snum' => $checkMatchData[$x]->Snum,
                                    'Bitem' => "$Bitm",
                                    'Mitem' => "$Mitm",
                                    'Taccount' => $checkMatchData[$x]->Taccount,
                                    'score' => 0,
                                    'block' => -1,
                                    'login' => "2"
                                );
                                $insert_data2[$moveonG++] = $gradedata;
                            }
                        }
                        $tempdata = array(
                            'department' => session()->get('department'),
                            'Bitem' => (string)($Bitm - 1),
                            'Mitem' => (string)($Mitm),
                            'Sitem' => "$Sitm",
                            'title' => "-1",
                            'HighScore' => null,
                            'LowScore' => null,
                            'login' => "2",
                            'percent' => (int)str_replace("%", "", $percent),
                            'project' => $project

                        );
                        $insert_data[$count] = $tempdata;
                        $Mitm++;
                        $Sitm++;
                    }else {
                        $title = str_replace("\n", "<br>", str_replace("\t", "", $Sitem[$moveon]));
                        $high = explode("~", $range[$moveon])[0];
                        $low = explode("~", $range[$moveon])[1];
                        $tempdata = array(
                            'department' => session()->get('department'),
                            'Bitem' => (string)($Bitm - 1),
                            'Mitem' => (string)($Mitm - 1),
                            'Sitem' => "$Sitm",
                            'title' => $title,
                            'HighScore' => $high,
                            'LowScore' => $low,
                            'login' => "2",
                            'percent' => 0,
                            'project' => $project
                        );
                        $insert_data[$count] = $tempdata;
                        $Sitm++;
                    }
                }
            }
        }
        //$allmark = DB::table('mark_data')->select('*')->where('department','=',session()->get('department'))->where('login','=','2')->get();
        //$xxxxx = DB::table('grades')->select('*')->where('department','=',session()->get('department'))->where('login','=','2')->get();
        try {
            if(isset($insert_data2)) {
                DB::table('grades')->insert($insert_data2);
            }
            if(isset($insert_data)) {
                DB::table('mark_data')->where('department','=',session()->get('department'))->where('login','=','2')->delete();
                DB::table('mark_data')->insert($insert_data);
            }
            cache()->put('datastatus', '匯入尺規成功!', 3);
            return back();
        } catch (QueryException $exception) {
            cache()->put('datastatus', '錯誤!，錯誤代碼:' . $exception->getCode() . "請洽維修人員", 3);
            return back();
        }
    }
}

