<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Grade;
use App\Match;
use App\Teacher;
use App\Flight;
use \Cache;
use function MongoDB\BSON\toJSON;
use Illuminate\Support\Facades\Hash;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (session()->get('status')=="office" ) {
            //cookie="bwerqer123123"
            return response(view('Department.teacher'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
        } else {
            setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
            $url = "https://sso.nuu.edu.tw/api/logout.php";
            $data_array = array("account" => session()->get('account'));
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data_array)
                )
            );
            $context  = stream_context_create($options);
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }

        // return view('test');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //必丟Account date 可丟 name
        $department = session()->get('department');
        $back = $_SERVER["HTTP_REFERER"];
        $flight['Faccount'] = $request->account;
        $flight['login'] = 'teacher';
        $flight['department'] = $department;
        if ($request->name != null) $flight['Fname'] = $request->name;
        if( Flight::where('department',$department)->where('login','teacher')->where('Faccount',$flight['Faccount'])->doesntExist() ){
            Flight::insert($flight);
        }
        $teacher['Taccount'] = $request->account;
        $teacher['login'] = '2';
        $teacher['department'] = $department;
        if ($request->name != null) $teacher['Tname'] = $request->name;
        if(Teacher::where('Taccount',$request->account)->where('department',$department)->whereIn('login', [ 2, 3])->exists()){
            Cache::put('datastatus', '老師帳號已存在' , 3);
        }else{
            Teacher::insert($teacher);
            Cache::put('datastatus', '儲存成功' , 3);
        }
        return redirect()->route('teacher.index');
       
        //一個不行全部不行

        // return redirect()->route('teacher.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        if(session()->get('status')=='office'){
            $department = session()->get('department');
            $users = DB::table('teachers')->where('department', $department)->whereIn('login', [ 2, 3])->get();
            return json_encode($users);
        }

        return json_encode([]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // 傳進teacher表裡的id 在從teacher取出Taccount Taccount找Faccount Faccount找flights的id
        $Taccount = Teacher::where('id', $request->id)->value('Taccount');
        $department=session()->get('department');
        if ($request->name != null) {
            // 只會改teacher表的
            // if(Flight::where('Faccount', $request->account)->exists() &&  !isset($request->account) ){
            //     Flight::where('Faccount',$Taccount)->where('department',$department)->update(['Fname' => $request->name]);
            // }
            Teacher::where('id',$request->id)->update(['Tname' => $request->name]);
        }
        if ($request->account != null) {
            if(Flight::where('Faccount', $request->account)->where('department',$department)->where('login','teacher')->doesntExist()){
                $flight['Faccount'] = $request->account;
                $flight['department'] = $department;
                $flight['login'] = 'teacher'; 
                if($request->name != null)$flight['Fname'] = $request->name; 
                Flight::insert($flight);
            }else{
                Flight::where('Faccount',$Taccount)->where('department',$department)->where('login','teacher')
                    ->update(['Faccount' => $request->account]);
            }
            Teacher::where('id',$request->id)->update(['Taccount' => $request->account]);
            Match::where('Taccount',$Taccount)->where('department', $department)->whereIn('login', [ 2, 3])->update(['Taccount' => $request->account]);
            Grade::where('Taccount',$Taccount)->where('department', $department)->whereIn('login', [ 2, 3])->update(['Taccount' => $request->account]);
        }
        
        Cache::put('datastatus', '更新成功' , 3);
        return redirect()->route('teacher.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        $department=session()->get('department');
        $Taccount=Teacher::where('id', $request->id)->value('Taccount');
        $back = $_SERVER["HTTP_REFERER"];
        Teacher::where('id', $request->id)->delete();
        if(Match::where('department', $department)->whereIn('login', [ 2, 3])->where('Taccount',$Taccount)->exists()){
            Match::where('department', $department)->whereIn('login', [ 2, 3])->delete();
            Grade::where('department', $department)->whereIn('login', [ 2, 3])->delete();
            Cache::put('datastatus', '成功刪除評分老師，請重新分配老師及評分' , 3);
        }else{
            Cache::put('datastatus', '成功刪除評分老師' , 3);
        }
        return redirect()->route('teacher.index');
        
        
        // return redirect()->route('teacher.index');
    }
    
}
