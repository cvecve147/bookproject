<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use function MongoDB\BSON\toJSON;
use Illuminate\Support\Facades\DB;
use App\MarkData;
use App\Student;
use App\StudentDepartment;
use App\Grade;
use App\Match;
use App\Department;
use \Cache;
use Illuminate\Support\Facades\Hash;

class JudgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (session()->get('status') == "teacher") {
            //cookie="bwerqer123123"
            return response(view('Teacher.index'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
            //return view('test');
        } else {
            setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
            $url = "https://sso.nuu.edu.tw/api/logout.php";
            $data_array = array("account" => session()->get('account'));
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data_array)
                )
            );
            $context  = stream_context_create($options);
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }
        // return view('test');  
        // return response(view('Teacher.index'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //待做權限
        //評分書審資料
        $Taccount = session()->get('account');
        $department = session()->get('department');
        // $Taccount='T1234';
        // $department='資訊管理學系';
        $Rating_item = []; 
        $all_Bitems = [];
        
        if(session()->get('status')=='teacher'){
            
        $Rating_item['all_item']=array();
        $Bitem = MarkData::where('department', $department)->where('Mitem', '0')->where('Sitem', '0')->where('login', '2')->pluck('id');
        $o = -1;
        foreach ($Bitem as $a) {
            $BitemNum = MarkData::where('id', $a)->value('Bitem');
            // dump($BitemNum);
            $items['ItemsTitle'] = MarkData::where('id', $a)->value('title');
            $items['ItemsNumber'] = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Sitem', '0')->where('login', '2')->count();
            $items['percent'] = (int)MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', '0')->where('login', '2')->where('Sitem', '0')->value('percent');
            $Mitem = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Sitem', '0')->where('login', '2')->pluck('id');
            $n = -1;
            $itemdata = null;
            foreach ($Mitem as $b) {
                $MitemNum = MarkData::where('id', $b)->value('Mitem');
                $item['number'] = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', $MitemNum)->where('Sitem', '!=', '0')->where('login', '2')->count();
                $Sitem = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', $MitemNum)->where('Sitem', '!=', '0')->where('login', '2')->pluck('id');
                $m = -1;

                unset($ScoreRangedata);
                foreach ($Sitem as $c) {
                    $SitemNum = MarkData::where('id', $c)->value('Sitem');
                    $ScoreRange['content'] = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', $MitemNum)->where('Sitem', $SitemNum)->where('login', '2')->value('title');
                    $ScoreRange['max'] = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', $MitemNum)->where('Sitem', $SitemNum)->where('login', '2')->value('HighScore');
                    $ScoreRange['min'] = MarkData::where('department', $department)->where('Bitem', $BitemNum)->where('Mitem', $MitemNum)->where('Sitem', $SitemNum)->where('login', '2')->value('lowScore');
                    
                    $ScoreRangedata[++$m] = $ScoreRange;
                }
                if (isset($ScoreRangedata)) $item['ScoreRange'] = $ScoreRangedata;
                else {
                    $item['ScoreRange'] = [];
                }
                $itemdata[++$n] = $item;
            }
            if (isset($itemdata)) $items['item'] = $itemdata;
            $itemsdata[++$o] = $items;
        }
        if (isset($itemsdata)) {
            $Rating_item['all_item'] = $itemsdata;
        }else{
            $Rating_item['all_item'] = [];
        }
        // $r=0;
        //     $Mitemid=MarkData::where('department', $department)->where('Sitem', '0')->where('login', '2')->pluck('id');
        //     $y=-1;
        //     $all_Mitems = [];
        //     foreach ($Mitemid as $a) {
        //         $Bitem = MarkData::where('id', $a)->value('Bitem');
        //         $mitem = MarkData::where('id', $a)->value('Mitem');
        //         $countMitem=MarkData::where('department', $department)->where('Bitem', $Bitem)->where('Sitem', '0')->where('login', '2')->count();
        //         $all_item['percent']=( (int)(MarkData::where('department', $department)->where('Bitem', $Bitem)->where('Mitem', '0')->where('Sitem', '0')->where('login', '2')->value('percent')) )/$countMitem;
        //         $all_item['title'] = MarkData::where('id', $a)->value('title');
        //         $Sitemid = MarkData::where('department',$department)->where('Bitem', $Bitem)->where('Mitem', $mitem)->where('Sitem', '!=', '0')->where('login', '2')->pluck('id');
        //         $x = -1;
        //         unset($headers);
        //         foreach ($Sitemid as $b) {
        //             $header['content'] = MarkData::where('id', $b)->value('title');
        //             $header['score'] = array();
        //             array_push(
        //                 $header['score'],
        //                 (int)MarkData::where('id', $b)->value('HighScore'),
        //                 (int)MarkData::where('id', $b)->value('lowScore')
        //             );
        //             $headers[++$x] = $header;
        //         }
        //         if (isset($headers)) $all_item['header'] = $headers;
        //         $all_Mitems[++$y] = $all_item;
        //     }
           
        //     if (isset($all_Mitems)) {
        //         $Rating_item['all_item'] = $all_Mitems; //找不到老師便會出錯
        //     } else {
        //         $Rating_item['all_item'] = [];
        //     }


            //show學生資料   
            $students = match::where('department', $department)->where('Taccount', $Taccount)->where('choose', '1')->where('login', '2')->pluck('id');
            // dump($students);
            $z = -1;
            $Studentdata = [];
            foreach ($students as $c) {
                $Snum = match::where('id', $c)->value('Snum');
                $Student['Snum'] = $Snum;
                $Student['name'] = Student::where('Snum', $Snum)->where('login', '2')->value('Sname');
                $Student['income'] = mb_substr(Student::where('Snum', $Snum)->where('login', '2')->value('income'), 0, 1, 'UTF-8');
                $Student['identifity'] = mb_substr(Student::where('Snum', $Snum)->where('login', '2')->value('identifity'), 0, 1, 'UTF-8');
                $Student['graduation'] = Student::where('Snum', $Snum)->where('login', '2')->value('graduation');
                $Student['address'] = Student::where('Snum', $Snum)->where('login', '2')->value('address');
                $Student['category'] = Student::where('Snum', $Snum)->where('login', '2')->value('category');
                $Student['gender'] = Student::where('Snum', $Snum)->where('login', '2')->value('gender');
                $Student['schoolnum'] = Student::where('Snum', $Snum)->where('login', '2')->value('schoolnum');
                $Student['year'] = Student::where('Snum', $Snum)->where('login', '2')->value('year');
                // dump(StudentDepartment::where('Snum', $Snum)->where('login', '2')->pluck('department'));
                //取分數
                $Bitem = Grade::where('department', $department)->where('Mitem', '0')->where('Snum', $Snum)->where('Taccount', $Taccount)->whereIn('login', [ 2, 3])->orderBy('Bitem')->pluck('Bitem'); 
                $tscore = -1;
                $tid = -1;
                $tmax = -1;
                $tmin = -1;
                $sid=[];
                $score=[];
                $max=[];
                $min=[];
                foreach ($Bitem as $d) {
                    $Mitem = Grade::where('department', $department)->where('Bitem', $d)->where('snum', $Snum)->where('Taccount', $Taccount)->whereIn('login', [ 2, 3])->orderBy('Mitem')->pluck('Mitem');
                    foreach ($Mitem as $e) {
                        $temid = Grade::where('department', $department)->where('Bitem', $d)->where('Mitem', $e)
                            ->where('snum', $Snum)->where('Taccount', $Taccount)->whereIn('login', [ 2, 3])->value('id');
                        $temscore = Grade::where('id', $temid)->value('score');
                        
                        $sid[++$tid] = $temid;
                        $score[++$tscore] = $temscore;
                        //max min
                        $temblock = Grade::where('id', $temid)->value('block');
                        if($temblock==-1){
                            // $max[++$tmax] = (int)MarkData::where('department', $department)->max('HighScore');
                            // $min[++$tmin] = (int)MarkData::where('department', $department)->min('lowscore');
                            $max[++$tmax] = 0;
                            $min[++$tmin] = 0;
                        }else{
                            $temmax = (int)MarkData::where('department', $department)->where('Bitem', $d)->where('Mitem', $e)->where('Sitem', $temblock)
                                        ->where('login', '2')->value('HighScore');
                            $temmin = (int)MarkData::where('department', $department)->where('Bitem', $d)->where('Mitem', $e)->where('Sitem', $temblock)
                                        ->where('login', '2')->value('lowscore');
                            $max[++$tmax] = $temmax;
                            $min[++$tmin] = $temmin;
                        }
                    }
                }
                $Student['id'] = $sid;
                $Student['score'] = $score;
                $Student['max'] = $max;
                $Student['min'] = $min;


                $Studentdata[++$z] = $Student;
            }
            if (isset($Studentdata)) {
                $Rating_item['student'] = $Studentdata; //找不到老師便會出錯
            } else {
                $Rating_item['student'] = [];
            }
            $Rating_item['department'] = StudentDepartment::where('department',$department)->value('departmentid');
            $Rating_item['issetstandard']=true;
            $Rating_item['issetmatch']=true;
            $Rating_item['canjudge']=true;
            if(MarkData::where('department',$department)->where('Bitem','0')->where('Mitem','0')->where('Sitem','1')->where('login','2')->doesntExist()){
                $Rating_item['issetstandard'] = false;
            }
            if($Rating_item['all_item']==[]){
                $Rating_item['issetstandard'] = false;
            }
            // if(MarkData::where('department',$department)->where('Sitem','!=','0')->where('Highscore',null)->where('login','2')->doesntExist()){
            //     $Rating_item['issetstandard'] = false;
            // }
            // if(MarkData::where('department',$department)->where('Sitem','!=','0')->where('lowscore',null)->where('login','2')->doesntExist()){
            //     $Rating_item['issetstandard'] = false;
            // }
            if(MarkData::where('department',$department)->where('login','2')->doesntExist()){
                $Rating_item['issetstandard'] = false;
            }
            if(Match::where('department',$department)->where('login','2')->doesntExist()){
                $Rating_item['issetmatch']=false;
            }
            if(Grade::where('department', $department)->where('login','3')->exists()){
                $Rating_item['canjudge']=false;
            }
            // $Rating_item['issetstandard'] = MarkData::where('department',$department)->where('Bitem','0')->where('Mitem','0')->where('Sitem','0')->where('login','2')->doesntExist();
            // $Rating_item['issetmatch'] = Match::where('department',$department)->where('login','2')->exists();

            return json_encode($Rating_item);
        }
        return json_encode([]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //test
        
        // dd($request);
        
        $department = session()->get('department');
        foreach ($request->all() as $key => $value) {
            try {  
                Grade::where('id', $value["id"])->update(['score' => $value["score"]]);
                $Bitem= Grade::where('id', $value["id"])->value('Bitem');
                $Mitem= Grade::where('id', $value["id"])->value('Mitem');
                if($Sitem=MarkData::where('Bitem', $Bitem)->where('Mitem', $Mitem)->where('HighScore',$value["max"])->where('lowScore',$value["min"])->where('department', $department)->exists() ){
                    $Sitem=MarkData::where('Bitem', $Bitem)->where('Mitem', $Mitem)
                    ->where('HighScore',$value["max"])->where('lowScore',$value["min"])
                    ->where('department', $department)->value('Sitem');
                    Grade::where('id', $value["id"])->update(['block' => $Sitem]);
                }                
            } catch (\Throwable $th) {
                echo $th;
            }
        }
        echo "Ok";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }
}
