<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;
use App\MarkData;
use App\Student;
use App\Grade;
use App\Match;
use App\Teacher;
use App\StudentDepartment;
use \Cache;
use Illuminate\Support\Facades\Hash;


class GradeDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (session()->get('status') == "office" or session()->get('status') == "head" ) {
            //cookie="bwerqer123123"
            return response(view('Department.All_scores'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
            //return view('test');
        }  else {
            setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
            $url = "https://sso.nuu.edu.tw/api/logout.php";
            $data_array = array("account" => session()->get('account'));
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data_array)
                )
            );
            $context  = stream_context_create($options);
            session()->flush();
            session()->put('error', '權限不足 請重新登入');
            return view('welcome');
        }
        // return view('test');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {


        // $keys = DB::table("flights")->select('*')->where('login', '=', "office")->orWhere('login', '=', "head")->pluck("Faccount");
        // if ($request->cookie('key') != "") {
        //     // $request->cookie('key')
        //     foreach ($keys as $key => $value) {
        //         if (Hash::check($value, $request->cookie('key'))) {
                    // // The passwords match...
                    // $accountdata = DB::table("flights")->select('*')->where('Faccount', '=', $value)->get();
                    // $department = $accountdata[0]->department;
        if(session()->get('status')=="office"  or session()->get('status') == "head" ){
            $department = session()->get('department');
            $Mitemid = MarkData::where('department', $department)->where('Sitem', 0)->whereIn('login', [ 2, 3])->pluck('id');
            // dd($Mitemid);
            $y = -1;
            foreach ($Mitemid as $a) {
                $Bitem = MarkData::where('id', $a)->value('Bitem');
                $Mitem = MarkData::where('id', $a)->value('Mitem');
                $Sitem = MarkData::where('department', $department)->where('Bitem', $Bitem)->where('Mitem', $Mitem)->where('Sitem', '!=', '0')->whereIn('login', [ 2, 3])->pluck('id');
                if (count($Sitem) == 0) continue;
                $all_item['title'] = MarkData::where('id', $a)->value('title');

                // if($Mitem=='1')dd($Sitem);

                // dd($Bitem,$Mitem);
                $x = -1;
                unset($headers);
                foreach ($Sitem as $b) {
                    $header['content'] = MarkData::where('id', $b)->value('title');
                    $header['score'] = array();
                    array_push(
                        $header['score'],
                        MarkData::where('id', $b)->value('HighScore'),
                        MarkData::where('id', $b)->value('lowScore')
                    );

                    $headers[++$x] = $header;
                    $header = null;
                    // dd($header);
                }
                if (isset($headers)) $all_item['header'] = $headers;
                // unset($headers);
                $all_items[++$y] = $all_item;
                // dd($all_items);
            }
            // dd($all_items);
            if (isset($all_items)) $Rating_item['all_item'] = $all_items;
            // dd($Bitem);
            // $Rating_item['teachers'] = Teacher::where('department', $department)->pluck('Taccount');
            // $teachers = Teacher::where('department', $department)->orderBy('Tname')->pluck('Taccount');
            // $z=-1;
            // $Rating_item['teachers']=array();
            // foreach($teachers as $f){
            //     $teacher['Taccount']=$f;
            //     $teacher['Tname'] = Teacher::where('department', $department)->where('Taccount', $f)->value('Tname');
            //     $teacherdata[++$z]=$teacher;                       
            // }
            // $Rating_item['teachers']=$teacherdata;
            // dd(Teacher::where('department',$department)->pluck('Taccount'));
            // $students = Student::where('department', $department)->pluck('id');
            $Students = StudentDepartment::where('department', $department)->whereIn('login', [ 2, 3])->pluck('Snum');
            $z = -1;
            $Big = MarkData::where('department', $department)->where('Mitem', '0')->where('Sitem', '0')->orderBy('Bitem')->whereIn('login', [ 2, 3])->pluck('Bitem');
            $Studentdata=array();
            foreach ($Students as $c) {
                unset($Student);
                if (Grade::where('Snum', $c)->doesntExist()) continue;
                $Student['Snum'] = $c;
                $Student['name'] = Student::where('Snum', $c)->whereIn('login', [ 2, 3])->value('Sname');
                $scroe = null;
                $y = -1;
                unset($titlescore);
                $titlescore = array();
                unset($countTitle);
                foreach ($Big as $d) {
                    $Med = MarkData::where('department', $department)->where('Bitem', $d)->where('Sitem', '0')->whereIn('login', [ 2, 3])->orderBy('Mitem')->pluck('Mitem');
                    foreach ($Med as $e) {
                        // if($d==1)dd($Big,$Med);
                        if (Grade::where('department', $department)->where('Bitem', $d)->where('Mitem', $e)->where('Snum', $Student['Snum'])->whereIn('login', [ 2, 3])->doesntExist()) continue;
                        $Taccount = Grade::where('department', $department)->where('Bitem', $d)->where('Mitem', $e)->where('Snum', $Student['Snum'])->whereIn('login', [ 2, 3])->pluck('Taccount');
                        // if($c=='10097537')dd($Taccount);
                        foreach ($Taccount as $f) {
                            $dname=Teacher::where('department', $department)->where('Taccount', $f)->whereIn('login', [ 2, 3])->value('id');
                            $scroe[$dname] = Grade::where('department', $department)->where('Bitem', $d)->where('Mitem', $e)->where('Snum', $Student['Snum'])->where('Taccount', $f)->whereIn('login', [ 2, 3])->value('score');
                        }

                        $title = MarkData::where('department', $department)->where('Bitem', $d)->where('Mitem', $e)->where('Sitem', 0)->whereIn('login', [ 2, 3])->value('title');
                        $countTitle[$title] =  array();
                        array_push($countTitle[$title], $scroe);
                        array_push($titlescore, $countTitle);
                        unset($countTitle[$title]);
                    }
                }

                // $Student['scroe'] = array();
                // array_push($Student['scroe'], $titlescore);
                $Student['scroe'] = $titlescore;
                $Studentdata[++$z] = $Student;
            }
            $Rating_item['student'] = $Studentdata;
            $Rating_item['teacher']=Teacher::where('department', $department)->whereIn('login', [ 2, 3])->select('id','Tname')->get();
            // return urldecode(json_encode($header,  JSON_FORCE_OBJECT));
            return json_encode($Rating_item);
        }
        //         }
        //     }
        // }

        return json_encode([]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
