<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;
use App\Student;
use App\StudentDepartment;
use App\MarkData;
use App\Grade;
use App\Match;
use App\MatchNum;
use App\Department;
use \Cache;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;

class OfficeDeleteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // if (session()->get('status')=="IR" || session()->get('status')=="office") {
        //     //cookie="bwerqer123123"
        //     return response(view('IR.index'))->cookie('key', bcrypt(session()->get('account')), 120, null, null, false, false);
        // } else {
        //     setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
        //     $url = "https://sso.nuu.edu.tw/api/logout.php";
        //     $data_array = array("account" => session()->get('account'));
        //     $options = array(
        //         'http' => array(
        //             'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        //             'method'  => 'POST',
        //             'content' => http_build_query($data_array)
        //         )
        //     );
        //     $context  = stream_context_create($options);
        //     session()->flush();
        //     session()->put('error', '權限不足 請重新登入');
        //     return view('welcome');
        // }
        return view('test');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
    //    dd($allDepartment=Department::get());
        // if(session()->get('status')=="IR"){
            // dd($request->department);
            if(isset($request->project)&&isset($request->department)){
                // return project department
                // dd(2);
                $department = $request->department;
                $Mitemid = MarkData::where('department', $department)->where('Sitem', 0)->where('login', $request->project)->pluck('id');
                $y = -1;
                foreach ($Mitemid as $a) {
                    $Bitem = MarkData::where('id', $a)->value('Bitem');
                    $Mitem = MarkData::where('id', $a)->value('Mitem');
                    $Sitem = MarkData::where('department', $department)->where('Bitem', $Bitem)->where('Mitem', $Mitem)->where('Sitem', '!=', '0')->where('login', $request->project)->pluck('id');
                    if (count($Sitem) == 0) continue;
                    $all_item['title'] = MarkData::where('id', $a)->value('title');
                    $x = -1;
                    unset($headers);
                    foreach ($Sitem as $b) {
                        $header['content'] = MarkData::where('id', $b)->value('title');
                        $header['score'] = array();
                        array_push(
                            $header['score'],
                            MarkData::where('id', $b)->value('HighScore'),
                            MarkData::where('id', $b)->value('lowScore')
                        );

                        $headers[++$x] = $header;
                        $header = null;
                    }
                    if (isset($headers)) $all_item['header'] = $headers;
                    $all_items[++$y] = $all_item;
                }
                if (isset($all_items)) $Rating_item['all_item'] = $all_items;
                $Students = StudentDepartment::where('department', $department)->where('login',$request->project)->pluck('Snum');
                $z = -1;
                $Big = MarkData::where('department', $department)->where('Mitem', '0')->where('Sitem', '0')->orderBy('Bitem')->where('login', $request->project)->pluck('Bitem');
                $Studentdata=array();
                foreach ($Students as $c) {
                    unset($Student);
                    if (Grade::where('Snum', $c)->doesntExist()) continue;
                    $Student['Snum'] = $c;
                    $Student['name'] = Student::where('Snum', $c)->whereIn('login', [ 2, 3])->value('Sname');
                    $scroe = null;
                    $y = -1;
                    unset($titlescore);
                    $titlescore = array();
                    unset($countTitle);
                    foreach ($Big as $d) {
                        $Med = MarkData::where('department', $department)->where('Bitem', $d)->where('Sitem', '0')->where('login', $request->project)->orderBy('Mitem')->pluck('Mitem');
                        foreach ($Med as $e) {
                            if (Grade::where('department', $department)->where('Bitem', $d)->where('Mitem', $e)->where('Snum', $Student['Snum'])->where('login', $request->project)->doesntExist()) continue;
                            $Taccount = Grade::where('department', $department)->where('Bitem', $d)->where('Mitem', $e)->where('Snum', $Student['Snum'])->where('login', $request->project)->pluck('Taccount');
                            foreach ($Taccount as $f) {
                                $dname=Teacher::where('department', $department)->where('Taccount', $f)->where('login',$request->project)->value('id');
                                $scroe[$dname] = Grade::where('department', $department)->where('Bitem', $d)->where('Mitem', $e)->where('Snum', $Student['Snum'])->where('Taccount', $f)->where('login', $request->project)->value('score');
                            }

                            $title = MarkData::where('department', $department)->where('Bitem', $d)->where('Mitem', $e)->where('Sitem', 0)->where('login', $request->project)->value('title');
                            $countTitle[$title] =  array();
                            array_push($countTitle[$title], $scroe);
                            array_push($titlescore, $countTitle);
                            unset($countTitle[$title]);
                        }
                    }

                    $Student['scroe'] = $titlescore;
                    $Studentdata[++$z] = $Student;
                }
                $Rating_item['student'] = $Studentdata;
                $Rating_item['teacher']=Teacher::where('department', $department)->where('login',$request->project)->select('id','Tname')->get();
                return json_encode($Rating_item);
            }else if(isset($request->department)){
                    // return project department
                    // dd(3);
                    $allproject['project']=MarkData::where('department',$request->department)->select('project')->distinct()->get();
                    $allproject['department']=$request->department;
                    return json_encode($allproject);
                
            }else{
                //return all department
                $allDepartment=Department::get();
                // dd('1',$allDepartment);
                return json_encode($allDepartment);
            }
        // }            
        
        return json_encode([]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $department=session()->get('department');
        $login=MarkData::where('department',$department)->where('login',2)->value('project');
        Teacher::where('department',$department)->where('login','2')->update(['login' => $login]);
        MarkData::where('department',$department)->where('login','2')->update(['login' => $login]);
        Grade::where('department',$department)->whereIn('login', [ 2, 3])->update(['login' => $login]);
        Match::where('department',$department)->where('login','2')->update(['login' => $login]);
        MatchNum::where('department',$department)->where('login','2')->update(['login' => $login]);
        StudentDepartment::where('department',$department)->where('login','2')->update(['login' => $login]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
