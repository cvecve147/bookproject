<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class compareController extends Controller
{
    function index(){
        if (session()->get('status')=="IR") {
            return response(view('IR.standard'))->cookie('key', bcrypt(session()->get('account')), 60, null, null, false, false);
        }
        switch (session()->get('status')) {
            case 'Admin':
                return redirect('/admin');
            case 'head':
                return redirect('/detail');
            case 'teacher':
                return redirect('/judge');
            case 'office':
                return redirect('/head');
            default:
                return  redirect('Undefined');
        }
        setcookie("token", "", time() - 1, "/", "nuu.edu.tw");
        $url = "https://sso.nuu.edu.tw/api/logout.php";
        $data_array = array("account" => session()->get('account'));
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data_array)
            )
        );
        $context  = stream_context_create($options);
        session()->flush();
        session()->put('error', '權限不足 請重新登入');
        return view('welcome');
    }
    function count($request){
        if (session()->get('status') == 'IR' or session()->get('status') == 'head') {
            if ($request->department == null or $request->dif == null) {
                $department = DB::table('teacher_standard_deviation')->select('department')->distinct()->get();

                return json_encode($department);
            } else if ($request->department != null and $request->get('dif') != null) {
                $student = DB::table('teacher_standard_deviation')->select('Snum', 'name')->where('department', $request->department)->distinct()->get();
                $teacher = DB::table('teacher_standard_deviation')->select('Taccount')->where('department', $request->department)->distinct()->get();
                $warnstudent = [];
                foreach ($student as $key => $value) {
                    $objs = [];
                    $max = 0;
                    $min = 9999;
                    for ($y = 0; $y < count($teacher); $y++) {
                        $obj = [];
                        $obj["sum"] = round(DB::table('teacher_standard_deviation')->select('score')->where('Taccount', $teacher[$y]->Taccount)->where('Snum', $value->Snum)->where('department', $request->department)->sum('score'), 2);
                        $max = max($max, $obj["sum"]);
                        $min = min($min, $obj["sum"]);
                        $obj["teacher"] = $teacher[$y]->Taccount;
                        array_push($objs, (object)$obj);
                        unset($obj);
                    }
                    if ($max - $min >= (int)$request->dif) {
                        array_push($warnstudent, (object)[
                            'studentname' => $value->name,
                            'studentSnum' => $value->Snum,
                            'teacher' => $objs
                        ]);
                    }
                    unset($objs);
                }
                return json_encode($warnstudent);
            }
        }else{
            if ($request->dif == null) {
                cache()->put('error', '請給差分分數(dif)', 3);
                return back();
            } else{
                $student = DB::table('teacher_standard_deviation')->select('Snum', 'name')->where('department', $request->department)->distinct()->get();
                $teacher = DB::table('teacher_standard_deviation')->select('Taccount')->where('department', $request->department)->distinct()->get();
                $warnstudent = [];
                foreach ($student as $key => $value) {
                    $objs = [];
                    $max = 0;
                    $min = 9999;
                    for ($y = 0; $y < count($teacher); $y++) {
                        $obj = [];
                        $obj["sum"] = round(DB::table('teacher_standard_deviation')->select('score')->where('Taccount', $teacher[$y]->Taccount)->where('Snum', $value->Snum)->where('department', $request->department)->sum('score'), 2);
                        $max = max($max, $obj["sum"]);
                        $min = min($min, $obj["sum"]);
                        $obj["teacher"] = $teacher[$y]->Taccount;
                        array_push($objs, (object)$obj);
                        unset($obj);
                    }
                    if ($max - $min >= (int)$request->dif) {
                        array_push($warnstudent, (object)[
                            'studentname' => $value->name,
                            'studentSnum' => $value->Snum,
                            'teacher' => $objs
                        ]);
                    }
                    unset($objs);
                }
                return json_encode($warnstudent);
            }
        }
    }

    function count2($req){
        $status=session()->get('status');
        if($status!="IR"){
            $req->department=session()->get("department");
        }

        if($status=="IR"){
            if($req->department== null or $req->dif== null){
                $departments = DB::table('student_departments')->select('department', 'departmentid')->distinct()->get();
                return json_encode($departments);
            }
        }
        //抓百分比 {0:50,1:50}
        $percent=DB::table("mark_data")
                    ->select('department',"Bitem","percent")
                    ->where('department', $req->department)
                    ->where("percent","!=","0")
                    ->get();
        $searchP=[0,0,0,0,0,0,0,0,0,0,0,0,0];
        foreach ($percent as $key => $value) {
            $searchP[intval($value->Bitem)]=$value->percent;
        }

        $grades=DB::table("grades")
                    ->join('students', 'students.Snum', '=', 'grades.Snum')
                    ->select('students.Snum', 'students.Sname','grades.Taccount', 'grades.Bitem', 'grades.score', 'grades.department')
                    ->where('department', $req->department)
                    ->orderBy("Snum","asc")->orderBy("Taccount","asc")->get();
        $group=array();
        foreach ($grades as $value) {
            $score=round($value->score*$searchP[intval($value->Bitem)]/100,3);
            $position=array_search($value->Snum,array_column($group, 'Snum'));
            if($position===false){
                $count=count($group);
                $group[$count]=array();
                $group[$count]["Snum"]=$value->Snum;
                $group[$count]["Sname"]=$value->Sname;
                $group[$count]["Teachers"]=array();
                $position=$count;
            }
            $TeacherP=array_search($value->Taccount,array_column($group[$position]["Teachers"], 'account'));
            if($TeacherP===false){
                $count=count($group[$position]["Teachers"]);
                $group[$position]["Teachers"][$count]=array();
                $group[$position]["Teachers"][$count]["account"]=$value->Taccount;
                $TeacherP=$count;
                $group[$position]["Teachers"][$TeacherP]["score"]=$score;
                continue;
            }
            $group[$position]["Teachers"][$TeacherP]["score"]+=$score;
            $group[$position]["Teachers"][$TeacherP]["score"]=round($group[$position]["Teachers"][$TeacherP]["score"],3);
        }
        $maxTeacherCount=0;
        $data=array();
        foreach($group as $student){
            $objs = [];
            $max = 0;
            $min = 9999;
            $maxTeacherCount=max(count($student["Teachers"]),$maxTeacherCount);
            foreach($student["Teachers"] as $item){
                $max=max($item["score"],$max);
                $min=min($item["score"],$min);
            }
            if($max-$min>=$req->dif){
                array_push($data,$student);
            }
            unset($objs);
        }
        $Tdata=array();
        array_push($Tdata,$data);
        array_push($Tdata,$maxTeacherCount);
        // var_dump($test);
        return json_encode($Tdata);
    }
    function show(Request $request){
        return $this->count2($request);
    }

}
