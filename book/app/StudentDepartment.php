<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentDepartment extends Model
{
    //
    protected $table = "student_departments";
    protected $fillable = [
        'Snum','department','login'
    ];
}
