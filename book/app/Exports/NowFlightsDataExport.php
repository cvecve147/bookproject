<?php

namespace App\Exports;

use App\NewDataExport;
use function Couchbase\defaultDecoder;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class NowFlightsDataExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private  $functionchoose;
    public function __construct($i)
    {
        $this->functionchoose = $i;
    }

    public function collection()
    {
        switch ($this->functionchoose){
            case 0://老師
                $data = DB::table('teachers')->select('*')->where('department','=',session()->get('department'))->where('login','=','2')->get();
                $celldata[0][0] = "帳號";
                $celldata[0][1] = "姓名";
                for ($x = 1,$y=0;$y<count($data);$x++,$y++){
                    $celldata[$x][0] = $data[$y]->Taccount;
                    $celldata[$x][1] = $data[$y]->Tname;
                }
                break;
            case 1://主任
                $data = DB::table('flights')->select('*')->where('department','=',session()->get('department'))->where('login','=','head')->get();
                $celldata[0][0] = "帳號";
                $celldata[0][1] = "姓名";
                for ($x = 1,$y=0;$y<count($data);$x++,$y++){
                    $celldata[$x][0] = $data[$y]->Faccount;
                    $celldata[$x][1] = $data[$y]->Fname;
                }
                break;
            case 2://系辦
                $data = DB::table('flights')->select('*')->where('login','=','office')->get();
                $celldata[0][0] = "帳號";
                $celldata[0][1] = "姓名";
                $celldata[0][2] = "系所";
                for ($x = 1,$y=0;$y<count($data);$x++,$y++){
                    $celldata[$x][0] = $data[$y]->Faccount;
                    $celldata[$x][1] = $data[$y]->Fname;
                    $celldata[$x][2] = $data[$y]->department;
                }
                break;
            case 3://學生
                $datacheck=DB::table('flights')->select('*')->where('Faccount','=',session()->get('account'))->get();
                if ($datacheck[0]->login == "IR"){
                    $data = DB::table('student_departments')->join('students', 'student_departments.Snum', '=', 'students.Snum')->select('students.*','student_departments.department','student_departments.departmentid')->where('student_departments.login','=','2')->orderByDesc('students.category')->orderBy('students.Snum','asc')->get();
                    $celldata[0][0] = "校系代碼";
                    $celldata[0][1] = "校系名稱";
                    $celldata[0][2] = "名額類別";
                    $celldata[0][3] = "學測應試號碼";
                    $celldata[0][4] = "姓名";
                    $celldata[0][5] = "性別";
                    $celldata[0][6] = "考生身分";
                    $celldata[0][7] = "低收入戶註記";
                    $celldata[0][8] = "通訊地址";
                    $celldata[0][9] = "畢業年度";
                    $celldata[0][10] = "學校代碼";
                    $celldata[0][11] = "畢業學校";
                    for ($x = 2, $y = 0; $y < count($data); $x++, $y++) {
                        $celldata[$x][0] = $data[$y]->departmentid;
                        $celldata[$x][1] = $data[$y]->department;
                        $celldata[$x][2] = $data[$y]->category;
                        $celldata[$x][3] = $data[$y]->Snum;
                        $celldata[$x][4] = $data[$y]->Sname;
                        $celldata[$x][5] = $data[$y]->gender;
                        $celldata[$x][6] = $data[$y]->identifity;
                        $celldata[$x][7] = $data[$y]->income;
                        $celldata[$x][8] = $data[$y]->address;
                        $celldata[$x][9] = $data[$y]->year;
                        $celldata[$x][10] = $data[$y]->schoolnum;
                        $celldata[$x][11] = $data[$y]->graduation;
                    }
                    break;
                }else {
                    $data = DB::table('student_departments')->join('students', 'student_departments.Snum', '=', 'students.Snum')->where('student_departments.login','=','2')->select('students.*', 'student_departments.department','student_departments.departmentid')->where('student_departments.department', '=', session('department'))->orderByDesc('students.category')->orderBy('students.Snum','asc')->get();
                    $celldata[0][0] = "校系代碼";
                    $celldata[0][1] = "校系名稱";
                    $celldata[0][2] = "名額類別";
                    $celldata[0][3] = "學測應試號碼";
                    $celldata[0][4] = "姓名";
                    $celldata[0][5] = "性別";
                    $celldata[0][6] = "考生身分";
                    $celldata[0][7] = "低收入戶註記";
                    $celldata[0][8] = "通訊地址";
                    $celldata[0][9] = "畢業年度";
                    $celldata[0][10] = "學校代碼";
                    $celldata[0][11] = "畢業學校";
                    for ($x = 2, $y = 0; $y < count($data); $x++, $y++) {
                        $celldata[$x][0] = $data[$y]->departmentid;
                        $celldata[$x][1] = $data[$y]->department;
                        $celldata[$x][2] = $data[$y]->category;
                        $celldata[$x][3] = $data[$y]->Snum;
                        $celldata[$x][4] = $data[$y]->Sname;
                        $celldata[$x][5] = $data[$y]->gender;
                        $celldata[$x][6] = $data[$y]->identifity;
                        $celldata[$x][7] = $data[$y]->income;
                        $celldata[$x][8] = $data[$y]->address;
                        $celldata[$x][9] = $data[$y]->year;
                        $celldata[$x][10] = $data[$y]->schoolnum;
                        $celldata[$x][11] = $data[$y]->graduation;
                    }
                    break;
                }
        }
        return collect($celldata);
    }
}
