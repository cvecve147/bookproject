<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Traits\HasCompositePrimaryKey;
class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            
            $table->bigIncrements('id'); //Account
            $table->string("Snum")->unique(); //學測應試號碼
            $table->string("Sname");
            $table->string("category");//名額類別
            $table->string("income");//收入
            $table->string("identifity");//身分
            $table->string("gender");//性別
            $table->string("schoolnum");//學校代碼
            $table->string("graduation");//畢業學校
            $table->string("year");//畢業年度
            $table->string("address");//地址
            $table->string("login");
            // $table->unique(array('Snum','department'));
            // $table->dropPrimary('id');
            // $table->primary('Snum');
            // $table->index(['Snum', 'department']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
