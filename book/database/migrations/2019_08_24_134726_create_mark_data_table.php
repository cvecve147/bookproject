<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarkDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mark_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("department");
            $table->string("Bitem");    //0
            $table->string("Mitem");    //0
            $table->string("Sitem");    //0
            $table->longText("title")->nullable();  //標題
            $table->integer("HighScore")->nullable();
            $table->integer("lowScore")->nullable();
            $table->string("login");
            $table->string("project");
            $table->integer("percent")->nullable();  //%
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mark_data');
    }
}
