<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("department");
            $table->string("Snum");
            $table->foreign("Snum")->references('Snum')->on('students')->onUpdate('cascade')->onDelete('cascade');
            $table->string("Bitem");
            $table->string("Mitem");
            $table->string("Taccount");
            $table->integer("score")->nullable();//0
            $table->string("block")->nullable();//-1
            $table->string("login");//2
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grades');
    }
}
