<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherStandardDeviationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_standard_deviation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("Taccount");
            $table->string("department");
            $table->string("reviewClass");
            $table->string("quotaClass");
            $table->string("Snum");
            $table->string("name");
            $table->string("sex");
            $table->string("Bitem");
            $table->string("score");
            $table->string("Remark")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_standard_deviation');
    }
}
