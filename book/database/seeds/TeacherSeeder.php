<?php

use Illuminate\Database\Seeder;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $Teacher = [
            ['Tname'=>'','department'=>"im","Taccount"=>"T123456","date"=>"2019-08-12"],
            ['Tname'=>'T123123','department'=>"im","Taccount"=>"T123457","date"=>"2019-08-12"],
            ['Tname'=>'T324246','department'=>"im","Taccount"=>"T123458","date"=>"2019-08-12"],
            ['Tname'=>'T233521','department'=>"im","Taccount"=>"T123459","date"=>"2019-08-12"],
            ['Tname'=>'T344352','department'=>"im","Taccount"=>"T123451","date"=>"2019-08-12"],
            ['Tname'=>'T123123','department'=>"im","Taccount"=>"T123453","date"=>"2019-08-12"]
        ];
        foreach ($Teacher as $Teachers) {
            DB::table('teachers')->insert($Teachers);
        }


    }
}
