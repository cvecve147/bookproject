<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [['Faccount' => "U0633117", 'name' => "陳慶鴻"], ['Faccount' => "U0633010", 'name' => "許茵筑"], ['Faccount' => "U0633126", 'name' => "余嘉軒"]];
        foreach ($datas as $data) {
            DB::table('flights')->insert([
                'Faccount' => $data['Faccount'],
                'Fname' => $data['name'],
                'position' => 'S',
                'department' => '資訊管理學系',
                'login' => 'Admin',
            ]);
        }
         $irdata = [['Faccount' => "ting978", 'name' => "許淑婷"],['Faccount' => "lfchen", 'name' => "陳立芬"]];
         foreach ($irdata as $data) {
             DB::table('flights')->insert([
                 'Faccount' => $data['Faccount'],
                 'Fname' => $data['name'],
                 'position' => 'S',
                 'department' => '資訊管理學系',
                 'login' => 'IR',
             ]);
         }
        // $datas = [['Faccount' => "Department", 'name' => "Test"]];
        // foreach ($datas as $data) {
        //     DB::table('flights')->insert([
        //         'Faccount' => $data['Faccount'],
        //         'Fname' => $data['name'],
        //         'position' => 'S',
        //         'department' => '資訊管理學系',
        //         'login' => 'office',
        //     ]);
        // }
        // $datas = [['Faccount' => "Teacher", 'name' => "Test"]];
        // foreach ($datas as $data) {
        //     DB::table('flights')->insert([
        //         'Faccount' => $data['Faccount'],
        //         'Fname' => $data['name'],
        //         'position' => 'S',
        //         'department' => '資訊管理學系',
        //         'login' => 'teacher',
        //     ]);
        // }
        // $datas = [['Faccount' => "head", 'name' => "Test"]];
        // foreach ($datas as $data) {
        //     DB::table('flights')->insert([
        //         'Faccount' => $data['Faccount'],
        //         'Fname' => $data['name'],
        //         'position' => 'S',
        //         'department' => '資訊管理學系',
        //         'login' => 'head',
        //     ]);
        // }
    }
}
