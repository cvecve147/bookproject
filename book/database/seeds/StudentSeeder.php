<?php

use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $Student = [
            ['Snum' => 'S043546', 'Sname' => '姓名1', 'department' => "im", "date" => "2020-08-12"],
            ['Snum' => 'S023123', 'Sname' => '姓名2', 'department' => "im", "date" => "2020-08-12"],
            ['Snum' => 'S024246', 'Sname' => '姓名3', 'department' => "im", "date" => "2020-08-12"],
            ['Snum' => 'S033521', 'Sname' => '姓名4', 'department' => "im", "date" => "2020-08-12"],
            ['Snum' => 'S044352', 'Sname' => '姓名5', 'department' => "im", "date" => "2020-08-12"],
            ['Snum' => 'S023124', 'Sname' => '姓名6', 'department' => "im", "date" => "2020-08-12"]
        ];
        foreach ($Student as $Students) {
            DB::table('students')->insert($Students);
        }
    }
}
