@extends('admin.allHeaders')
@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
  integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<style>
  .selected {
    background: #FFAA33;
    font-weight: bold;
  }

  .bg-infos {
    background: lightblue;
    color: black;
  }

  .buttons i {
    position: fixed;
    font-size: 50px;
    padding: 0;
    margin: 0;
    cursor: pointer;
    background-color: rgba(233, 159, 159, 0);
  }

  .up {
    position: fixed;
    bottom: calc(50% + 45px);
    right: 45px;
  }

  .down {
    position: fixed;
    bottom: calc(50%);
    right: 45px;
  }
</style>
<div id="score" @click="click">
  <div v-if="!loading">
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">設定 顯示資料</h5>
          </div>
          <div class="modal-body">
            <input type="checkbox" v-model="showStudentdata[0]">應試號碼</br>
            <input type="checkbox" v-model="showStudentdata[1]">名額類別</br>
            <input type="checkbox" v-model="showStudentdata[2]">姓名</br>
            <input type="checkbox" v-model="showStudentdata[3]">性別</br>
            <input type="checkbox" v-model="showStudentdata[4]">考生身分</br>
            <input type="checkbox" v-model="showStudentdata[5]">收入戶</br>
            <input type="checkbox" v-model="showStudentdata[6]">通訊地址區域</br>
            <input type="checkbox" v-model="showStudentdata[7]">畢業年度</br>
            <input type="checkbox" v-model="showStudentdata[8]">畢業學校</br>
            <input type="checkbox" v-model="showStudentdata[9]">學校編號</br>
            <input type="checkbox" v-model="showStudentdata[10]">書審資料</br>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <div class="buttons">
      <i class="fas fa-angle-up up" @click="Scrolldown(-1)"></i>
      <i class="fas fa-angle-down down" @click="Scrolldown(1)"></i>
    </div>
    <div class="d-flex justify-content-end align-items-center" style="font-size:28px;">
      <i class="fas fa-angle-left" @click="Scrolllleft(-1)"></i><span
        class="font-weight-bold mx-2">@{{nowjudge+1}}</span>/
      <span class="mx-2">@{{all_item.length}}</span><i class="fas fa-angle-right" @click="Scrolllleft(1)"></i>
    </div>
    <table class="table text-center">
      <thead>
        <tr>
          <th :colspan="countshowStudentData" v-if="countshowStudentData">基本資料 <button class="btn " data-toggle="modal"
              data-target="#exampleModal"><i class="fas fa-user-cog"></i></button></th>
          <template v-for="(item, all_item_index) in all_item" v-if="all_item_index==nowjudge">
            <th v-for="(detail, header_index) in item.header" colspan="2" v-html="detail.content"
              @click="clickGetScore(detail.score[0],detail.score[1],all_item_index)"
              style="word-break: break-all; width: 300px !important;" class="text-center"
              :class="{thboder:header_index==(item.header.length-1)}"></th>
          </template>
          <th colspan="2" class="bg-infos">
            總分
          </th>
        </tr>
        <tr>
          <th class="name" v-if="showStudentdata[0]">應試號碼</th>
          <th class="name" v-if="showStudentdata[1]">名額類別</th>
          <th class="name" v-if="showStudentdata[2]">姓名</th>
          <th class="name" v-if="showStudentdata[3]">性別</th>
          <th class="name" v-if="showStudentdata[4]">考生身分</th>
          <th class="name" v-if="showStudentdata[5]">收入戶</th>
          <th class="name" v-if="showStudentdata[6]">通訊地址區域</th>
          <th class="name" v-if="showStudentdata[7]">畢業年度</th>
          <th class="name" v-if="showStudentdata[8]">畢業學校</th>
          <th class="name" v-if="showStudentdata[9]">學校編號</th>
          <th class="name" v-if="showStudentdata[10]">書審資料</th>
          <template v-for="(item, all_item_index) in all_item" v-if="all_item_index==nowjudge">
            <template v-for="(detail, header_index) in item.header">
              <th v-for="(score, index) in detail.score" class="text-center scores  scorewidth"
                :class="{thboder:(header_index==(item.header.length-1) && index==1)}">
                @{{ score }}
              </th>
            </template>
          </template>
          <th class="name bg-infos">加總</th>
          <th class="name bg-infos">加權後</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for=" (item,student_index) in student" @click="selectStudent(student_index)"
          :class="{selected:student_index==selectStudents}" style="height: 50px;" v-if="studentShow[student_index]">

          <td class="name" v-if="showStudentdata[0]">@{{item.Snum}}</td>
          <td class="name" v-if="showStudentdata[1]">@{{item.category}}</td>
          <td class="name" v-if="showStudentdata[2]">@{{item.name}}</td>
          <td class="name" v-if="showStudentdata[3]">@{{item.gender}}</td>
          <td class="name" v-if="showStudentdata[4]">@{{item.identifity}}</td>
          <td class="name" v-if="showStudentdata[5]">@{{item.income}}</td>
          <td class="name" v-if="showStudentdata[6]">@{{item.address.substr(0,6)}}</td>
          <td class="name" v-if="showStudentdata[7]">@{{item.year}}</td>
          <td class="name" v-if="showStudentdata[8]">@{{item.graduation}}</td>
          <td class="name" v-if="showStudentdata[9]">@{{item.schoolnum}}</td>
          <td class="name" v-if="showStudentdata[10]"><a
              :href="`http://irmaterials.nuu.edu.tw/storage/student_data/${department}/${item.Snum}/${item.Snum}.pdf`"
              target="_blank">查看</a>
          </td>
          <template v-for="(item1, all_item_index1) in all_item" v-if="all_item_index1==nowjudge">
            <td :colspan="item_length[all_item_index1]" class="text-center">
              <el-input-number v-model="item.score[all_item_index1]" :min="item.min[all_item_index1]"
                :max="item.max[all_item_index1]" label=""></el-input-number>
            </td>
          </template>
          <td class="name  bg-infos">@{{ sumScore(student_index) }}</td>
          <td class="name  bg-infos">@{{ sumScore2(student_index) }}</td>
        </tr>
      </tbody>
    </table>
    <div class="container my-2" v-if="!await">
      <div class="row">
        <div class="col-12">
          <form action="{{ route('judge.update', 0) }}" method="POST" v-if="student">
            <input type="hidden" name="_method" value="PUT" />
            @csrf
            <div style="display:none">
              <template v-for="item in student">
                <template v-for="items in item.score.length">
                  <input type="number" name="id[]" v-model="item.id[items-1]" />
                  <input type="number" name="score[]" v-model="item.score[items-1]" />
                  <input type="number" name="max[]" v-model="item.max[items-1]" />
                  <input type="number" name="min[]" v-model="item.min[items-1]" />
                  <br />
                </template>
              </template>
            </div>
            <button class="btn btn-block btn-primary py-2">送出</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div v-else class="container" style="height:80vh">
    <div class="row">
      <div class="col-12 pt-5">
        <div class="bouncing-loader ">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  </div>
  <div v-if="await" class="container" style="height:80vh">
    <div class="row">
      <div class="col-12 d-flex flex-row ">
        <H1 class="text-center">請等待 系辦建置資料</H1>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
@parent

<script>
  new Vue({
      el: "#score",
      data() {
        return {
          all_item: [],
          student: [],
          item_length: [],
          loading: false,
          selectStudents: 0,
          department: "",
          await: false,
          showStudentdata:[],
          nowjudge:0,
          studentShow:[],
        };
      },
      computed: {
        countshowStudentData(){
            var count=0
            this.showStudentdata.forEach(el => {
              if(el==true)count++
            });
              return count
        },
        allitem() {
          return this.all_item.length;
        },
        sumScore() {
          return function(index) {
            var temp = 0;
            this.student[index].score.forEach(element => {
              temp += element;
            });
            return temp;
          };
        },
        
        sumScore2() {
          return function(studentindex) {
            var temp = 0;
            for (let index = 0; index < this.student[studentindex].score.length; index++) {
              temp+=((this.all_item[index].percent*0.01)*this.student[studentindex].score[index])
            }
            return temp.toFixed(2);
          };
        }, 
      },
      methods: {
        async click(){
            const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },          
        Scrolldown(val){
          if(val>0){
            if(this.studentShow[this.studentShow.length-1]!=true){
              this.studentShow.splice(0, 0, false);
              this.studentShow.pop()
            }
          }else{
            if(this.studentShow[0]!=true){
              this.studentShow.splice(this.studentShow.length, 0, false);
              this.studentShow.shift()
            }
          }
        },
        Scrolllleft(val){
          if(val>0){
            this.all_item.length==this.nowjudge+val ? this.$message({message: "到底了",type: "warning" ,duration:1000}): this.nowjudge+=val
          }else{
            -1==this.nowjudge+val ? this.$message({message: "最前面了!",type: "warning" ,duration:1000}): this.nowjudge+=val
          }
        },
        changePage(tab) {
          this.now = tab.index;
        },
        clickGetScore(max, min, i) {
          var score = max + min;
          this.$set(this.student[this.selectStudents].max, i, max);
          this.$set(this.student[this.selectStudents].min, i, min);
          this.$set(
            this.student[this.selectStudents].score,
            i,
            parseInt(score / 2)
          );
        },
        selectStudent(student_index) {
          this.selectStudents = student_index;
        },
        async sorts() {
          var All_total;
          for (let i = 0; i < this.student.length; i++) {
            const element = this.student[i];
            All_total = 0;
            element.score.forEach(el => {
              All_total += el;
            });
            this.student[i].All_total = All_total;
          }
          // this.student = this.student.sort(function(a, b) {
          //   return a.All_total > b.All_total ? 1 : -1;
          // });
          this.student = await this.student.sort(function(a, b) {
            return a.All_total < b.All_total ? 1 : -1;
          });
          this.$message({
            message: "排序完成",
            type: "success"
          });
        }
      },
      async created() {
        for (let i = 0; i < 11; i++) {
            this.showStudentdata.push(true)
          }
        this.loading = true;
        
        try {
          const res = await axios.get("http://irmaterials.nuu.edu.tw/judge/show");
          //const res = await axios.get("http://127.0.0.1:8000/judge/show");
          this.student = await res.data.student;
          this.department=await res.data.department;
          this.all_item = await res.data.all_item;
          this.await=!(res.data.issetstandard && res.data.issetmatch)
          
          if(this.await==true)reutrn;
          this.all_item.forEach(element => {
          this.item_length.push(element.header.length * 2);
          });
          this.student.forEach(el=>{
            this.studentShow.push(false)
          })
          for (let i = 0; i < 5; i++) { this.studentShow[i]=true }
          //console.log(this.all_item);
          
          this.loading = false;
        } catch (error) {
          alert("Server Error");
          this.loading = false;
          this.await=true;
        }      
      }
  })      
</script>
@endsection