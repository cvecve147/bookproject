@extends('admin.allHeaders')
@section('content')
<style>
    .el-transfer-panel {
        width: 300px
    }

    .el-transfer__buttons {
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 0 1em;
        margin: 0;
    }

    .el-transfer__buttons button {
        margin: 0 3px !important;
    }
</style>
<section id="select" @click="click">
    <div class="modal fade" id="InputForExcel" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">匯入</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row my-2">
                            <form action="{{route('matchesImporter')}}" method="post" enctype="multipart/form-data"
                                class="d-flex col-12 w-100">
                                @csrf
                                <input type="file" id="ExcelAdd" name="select_file" style="display: none"
                                    accept=".xlsx, .xls, .csv" v-on:change="onFileChange($event)" />
                                <div class="d-flex flex-row justify-content-between w-100">
                                    <div>
                                        <button type="button" class="btn btn-primary" onclick="$('#ExcelAdd').click()">
                                            匯入分配
                                        </button>
                                        <span class="mx-2 my-auto"><small>@{{addname}}</small></span>
                                    </div>
                                    <button type="submit" class="btn btn-success" v-if="addname!=''">送出</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <a href="/storage/Excel%20example/match.xlsx">範例檔案</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        關閉
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div v-if="loading">
        <div class="container">
            <div class="row">
                <form action="{{ route('match.destroy', 0) }}" method="post">
                    <div class="container w-100">
                        <div class="row">
                            <div class="col-4">
                            </div>
                            <div class="col-6 w-100">
                                <input type="hidden" name="_method" value="DELETE" />
                                @csrf
                                <div class="form-group w-100">
                                    <label for="">請輸入學生需要幾位老師評分</label>
                                    <input type="number" name="number" min="0" :max="max" class="form-control"
                                        placeholder="" v-model="number" aria-describedby="helpId">
                                    <small id="helpId" class="text-muted">第一次輸入後按下確定 資料庫產生資料 <span
                                            style="color:red;">若想更改人數
                                            資料庫不保留以選擇人員</span></small>
                                </div>
                            </div>
                            <div class="col-2" v-if="!hasjudge">
                                <label for="">確認送出</label>
                                <button type="submit" name="" class="btn btn-primary">送出</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container" v-if="hasjudge">
            <div class="row">
                <div class="col-12 d-flex flex-row justify-content-center">
                    <H3 class="text-danger">
                        老師已經進行評分，請勿修改分配表
                    </H3>
                </div>
            </div>
        </div>
        <div v-if="Teaher.length>0 && (typeof number != 'null')">
            <div class="text-center container" v-if="!hasjudge">
                <button type="button" class="btn btn-success mx-2" data-toggle="modal"
                    data-target="#InputForExcel">Excel匯入分配名單</button>
                <button class="btn btn-primary" @click="matchs">
                    自動分配
                </button>
            </div>
            <el-row type="flex" class="row-bg mt-2" justify="center">
                <el-col :lg="12" :xl="10">
                    <el-card v-for="(item1, index1) in Teaher" :key="index1">
                        <div slot="header">
                            <span>@{{ item1.Tname==''?item1.Taccount :item1.Tname }} &nbsp;&nbsp;&nbsp; 老師</span>
                        </div>
                        <el-transfer v-model="value[index1]" :props="{
                            key: 'value',
                            label: 'desc'
                            }" :key="index1" @change="identify" :titles="['未選擇', '已選擇']" :data="data[index1]"
                            class="d-flex align-items-center  justify-content-center">
                        </el-transfer>
                    </el-card>
                    <form action="{{ route('match.update', 0) }}" method="post">
                        <input type="hidden" name="_method" value="PUT" />
                        @csrf
                        <div v-for="(item, index) in value" :key="index" style="display:none;">
                            <input type="text" name="Taccount[]" class="btn btn-primary"
                                :value="Teaher[index].Taccount" />
                            <input type="text" name="Snum[]" class="btn btn-primary" :value="item" />
                        </div>
                        <div class="center mt-2" style="text-align: center " v-if="!hasjudge">
                            <el-button type="primary" size="medium" native-type="submit">送出</el-button>
                        </div>
                    </form>
                </el-col>
            </el-row>
        </div>
    </div>

    <div v-else class="container" style="height:80vh">
        <div class="row">
            <div class="col-12 pt-5">
                <div class="bouncing-loader ">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
@parent

<script>
    new Vue({
        el: "#select",
        data() {
            return {
                Teaher: [],
                max:10,
                data: [],
                value: [],
                number: 0,
                num: 1,
                loading: true,
                hasjudge:false,
                addname:""
            };
        },
        methods: {
        async click(){
            const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },
            onFileChange(e) {
                if (e.target.files.length != 0) {
                //console.log(e.target.files[0].name);
                this.addname = e.target.files[0].name;
                }
            },
            identify() {
                var result = {};

                for (let index = 0; index < this.data[0].length; index++) {
                    var item = this.data[0][index].value;
                    result[item] = result[item] ? result[item] + 1 : 0;
                }

                for (let index = 0; index < this.value.length; index++) {
                    this.value[index].forEach(function(item) {
                        result[item] = result[item] ? result[item] + 1 : 1;
                    });
                }
                Object.keys(result).forEach(el => {
                    if (this.number <= result[el]) {
                        for (let i = 0; i < this.Teaher.length; i++) {
                            for (let index = 0; index < this.data[i].length; index++) {
                                if (this.data[i][index].value == el) {
                                    this.data[i][index].disabled = true
                                }
                            }
                        }
                        for (let i = 0; i < this.value.length; i++) {
                            for (let index2 = 0; index2 < this.data[i].length; index2++) {
                                for (let index = 0; index < this.value[i].length; index++) {
                                    if (this.data[i][index2].value == this.value[i][index]) {
                                        this.data[i][index2].disabled = false
                                    }
                                }
                            }
                        }
                    } else {
                        for (let i = 0; i < this.Teaher.length; i++) {
                            for (let index = 0; index < this.data[i].length; index++) {
                                if (this.data[i][index].value == el) {
                                    this.data[i][index].disabled = false
                                }
                            }
                        }
                    }
                });
                this.max=this.Teaher.length;
            },
            async fetch() {
                this.loading = false
                const res = await axios.get("http://irmaterials.nuu.edu.tw/match/show");
                //   //console.log(res.data[0]);
                this.number = parseInt(res.data.number);
                this.Teaher = res.data.teacher;
                this.max=this.Teaher.length;
                if(res.data.teacher.length==0){
                    this.loading =true
                    this.$message({message: "請先建置老師資料",type: 'error',duration:3000})
                    return ;
                }
                if(typeof number == "null"){
                    this.loading =true
                    return ;
                }
                this.data = [];
                this.hasjudge=res.data.hasjudge;               
                for (let index = 0; index < this.Teaher.length; index++) {
                    this.data[index] = new Array();
                    this.value[index] = new Array();
                    if(res.data.Student2){                        
                        res.data.Student2.forEach(el => {
                        //   //console.log(el);
                            
                            this.$set(this.data[index], this.data[index].length, {
                                value: el.Snum,
                                desc: el.Snum + " " + el.Sname,                            
                                disabled: false
                            });
                        });      
                    }
                    
                }
                if (!res.data.SelectStudent) {} else {
                    res.data.SelectStudent.forEach(el => {
                        for (let i = 0; i < this.Teaher.length; i++) {
                            if (el.TAccount == this.Teaher[i].Taccount) {
                                el.Snum.forEach(el2 => {
                                    this.value[i].push(el2.Snum);
                                });
                            }
                        }
                    });
                }
                this.identify()
                if(this.hasjudge){
                    this.$message({
                        message: "老師已經進行評分，請勿修改分配表",
                        type: 'warning'
                    });
                    for (let i = 0; i < this.value.length; i++) {
                        for (let index2 = 0; index2 < this.data[i].length; index2++) {
                            for (let index = 0; index < this.value[i].length; index++) {
                                if (this.data[i][index2].value == this.value[i][index]) {
                                    this.data[i][index2].disabled = true
                                }
                            }
                        }
                    }
                }
                this.loading = true
            },
            async matchs() {
                if (this.number > this.Teaher.length) {
                    alert("請確認老師與學生需要幾位老師數量!!!")
                    return false;
                }
                if (!confirm("將會清除所有已選擇的學生")) {
                    return false;
                }

                var times;
                var now;
                this.value = []
                for (let i = 0; i < this.Teaher.length; i++) {
                    this.value[i] = new Array()
                }
                times = now = 0;
                await this.data[0].forEach(el => {
                    while (this.number > times) {
                        for (let i = now; i < this.Teaher.length; i++) {
                            if (this.number <= times) {
                                now = i;
                                break
                            } else {
                                times++;
                                this.value[i].push(el.value)
                            }
                            if (this.Teaher.length - 1 == i) {
                                now = 0;
                            }
                        }
                    }
                    times = 0;
                    if(el.value==this.data[0][this.data[0].length-1].value){
                        this.identify()

                    }
                });
                alert("按下最下面送出 保留已選擇名單")
            }
        },
        created() {
            this.fetch();
        }
    })
</script>
@endsection