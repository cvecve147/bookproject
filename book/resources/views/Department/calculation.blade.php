@section('content')
<style>
    .display {
        display: none;
    }

    table tbody {
        display: block;
        height: 60vh;
        overflow-y: scroll;
    }

    table thead,
    tbody tr {
        display: table;
        width: 100%;
        table-layout: fixed;
    }

    table thead {
        width: calc(100% - 1em);
    }

    .scores:nth-child(odd)::after {
        content: '~';
        display: inline-block;
        height: 100%;
        width: 50%;
        vertical-align: top;
        text-align: end;
        translate(50%, 50%);
    }
</style>
<div id="score" @click="click">
    <div class="d-flex flex-row justify-content-end">
        <a href="/deleteteacher/show?type=1">
            <button class="btn btn-warning m-2">
                關閉老師評分
            </button>
        </a>
        <a href="/deleteteacher/show?type=2">
            <button class="btn btn-success m-2">
                回復老師評分
            </button>
        </a>
        <a href="/excel/export">
            <button class="btn btn-primary m-2">
                下載Excel
            </button>
        </a>
    </div>
    <el-tabs type="border-card" @tab-click="changePage" v-if="!loading">
        <el-tab-pane :label="item.title" v-for="(item, index) in all_item" :key="index" :stretch="true">
            <table class="table">
                <thead class="sticky-top position-static">
                    <tr>
                        <th colspan="2">評分項目 與 學生</th>
                        <template v-for="(item, all_item_index) in all_item">
                            <th class="text-center" v-for="(detail, header_index) in item.header" colspan="2"
                                v-html="detail.content" :class="{ display: all_item_index!= now}"
                                style="word-break: break-all;"></th>
                        </template>
                    </tr>
                    <tr>
                        <th colspan="1">准考證號碼</th>
                        <th colspan="1">姓名</th>
                        <template v-for="(item, all_item_index) in all_item">
                            <template v-for="(detail, header_index) in item.header">
                                <th v-for="(score, index) in detail.score" class="text-center "
                                    :class="{ display: all_item_index!= now}">
                                    @{{ score }}
                                </th>
                            </template>
                        </template>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(item, student_index) in student">
                        <td scope="row" colspan="1">@{{ item.Snum }}</td>
                        <td scope="row" colspan="1">@{{ item.name }}</td>
                        <template v-for="(item1, all_item_index1) in all_item">
                            <template v-for="(item3,index)  in item.scroe[all_item_index1]">
                                <template v-for="(item4,index4) in item3">
                                    <template v-for="(item5,index5) in item4">
                                        <td :colspan="colspan(all_item_index1,item4)" class="text-center"
                                            :class="{ display : all_item_index1 != now}">@{{ teachername(index5) }} 老師 :
                                            @{{item5}}</td>

                                    </template>
                                </template>
                            </template>
                        </template>
                    </tr>
                </tbody>
            </table>
        </el-tab-pane>
    </el-tabs>
    <div v-else class="container" style="height:80vh">
        <div class="row">
            <div class="col-12 pt-5">
                <div class="bouncing-loader ">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
@parent

<!-- import JavaScript -->


<script>
    new Vue({
        el: "#score",
        data() {
            return {
                now: 0,
                all_item: [],
                student: [],
                item_length: [],
                loading: false,
                teacher:[]
            };
        },
        computed: {
            teachername(){
                return function (index) {
                    var name;
                    this.teacher.forEach(element => {
                        if (element.id==index) {
                            //console.log(element.id, element.Tname);
                            name=element.Tname
                        }
                    });
                    return name;

                }
            },
            allitem() {
                return this.all_item.length;
            },
            sumScore() {
                return function(index) {
                    var temp = 0;
                    this.student[index].score.forEach(element => {
                        temp += element;
                    });
                    return temp;
                };
            },
            colspan(){
                return function(index,items) {
                    var count=0
                    for (var prop in items) {
                        count++;
                    }                 
                    return this.item_length[index]/count
                };
            }
        },
        methods: {
        async click(){
            const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },
            changePage(tab) {
                this.now = tab.index;
            },

        },
        async created() {
            this.loading = true;
            const res = await axios.get("http://irmaterials.nuu.edu.tw/detail/All_scores");
            this.student = res.data.student;
            this.all_item = res.data.all_item ?res.data.all_item :[];
            this.teacher=res.data.teacher;
            if(this.all_item.length==0)this.$message({message: "請先建置系所相關資料",duration:3000})
            this.all_item.forEach(element => {
                this.item_length.push(element.header.length * 2);
            });
            //console.log(res.data);
            
            this.loading = false;
        }
    });
</script>
@endsection