@extends('admin.allHeaders')
@section('content')
<div class="modal fade" id="InputForExcel" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">匯入</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <div class="modal-body">
                <div class="container">
                    <div class="row my-2">
                        <form action="{{route('iptOfDepartment')}}" method="post" enctype="multipart/form-data"
                            class="d-flex">
                            @csrf
                            <input type="file" id="ExcelAdd" name="select_file" style="display: none"
                                accept=".xlsx, .xls, .csv" v-on:change="onFileChange($event)" />
                            <button type="button" class="btn btn-success" onclick="$('#ExcelAdd').click()">

                                新增
                            </button>
                            <span class="mx-2 my-auto">新增資料在原本資料後方，不會將原資料刪除<br><small>@{{addname}}</small></span>

                            <button type="submit" class="btn btn-success" v-if="addname!=''">送出</button>



                        </form>
                    </div>
                    <div class="row my-2">
                        <form action="{{route('IRDOupdate')}}" method="post" enctype="multipart/form-data"
                            class="d-flex">
                            @csrf
                            <input type="file" id="ExcelUpdate" name="select_file" style="display: none"
                                accept=".xlsx, .xls, .csv" v-on:change="onFileChange2($event)" />
                            <button type="button" class="btn btn-primary" onclick="$('#ExcelUpdate').click()">

                                更新
                            </button>
                            <span class="mx-2 my-auto">取代原本資料，不會保留原有資料<br><small>@{{upname}}</small></span>
                            <button type="submit" onclick="return confirm('請注意取代原本資料，『不會保留』原有資料!!!');"
                                class="btn btn-primary" v-if="upname!=''">送出</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <a href="/storage/Excel%20example/office.xlsx">範例檔案</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    關閉
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="TypingInput" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">新增系辦資料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('flight.store')}}" method="post">
                    @csrf
                    <div class="container">
                        <div class="row">
                            <div class="form-group">
                                <label for="">帳號</label>
                                <input type="text" name="Faccount" class="form-control" placeholder=""
                                    aria-describedby="helpId" v-model="type.account" required />
                                <small id="helpId" class="text-muted">輸入學校帳號</small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="">姓名</label>
                                <input type="text" name="Fname" class="form-control" placeholder=""
                                    aria-describedby="helpId" v-model="type.name" />
                                <small id="helpId" class="text-muted">輸入名稱 可為空值</small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 px-0">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputGroupSelect01">系組</label>
                                    </div>
                                    <select class="custom-select" id="inputGroupSelect01" v-model="type.department"
                                        name="department" required>
                                        <template v-for="item in departments">
                                            <option :value="item.department">@{{ item.department}} -
                                                @{{item.departmentid}}</option>
                                        </template>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 p-0 mt-0">
                                <small id="helpId" class="text-muted">請選擇系組</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="CloseInput">
                            關閉
                        </button>
                        <button type="submit" class="btn btn-primary">儲存</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="changeinput" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">修改系辦資料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('flight.update',0)}}" method="post">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="id" v-model="change.id">
                    @csrf
                    <div class="container">
                        <div class="row">
                            <div class="form-group">
                                <label for="">帳號</label>
                                <input type="text" name="Faccount" class="form-control" placeholder=""
                                    aria-describedby="helpId" v-model="change.account" required />
                                <small id="helpId" class="text-muted">輸入學校帳號</small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="">姓名</label>
                                <input type="text" name="Fname" class="form-control" placeholder=""
                                    aria-describedby="helpId" v-model="change.name" />
                                <small id="helpId" class="text-muted">輸入名稱</small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 px-0">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputGroupSelect01">系組</label>
                                    </div>
                                    <select class="custom-select" id="inputGroupSelect01" v-model="change.department"
                                        name="department" required>
                                        <template v-for="item in departments">
                                            <option :value="item.department">@{{ item.department}} -
                                                @{{item.departmentid}}</option>
                                        </template>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 p-0 mt-0">
                                <small id="helpId" class="text-muted">請選擇系組</small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="CloseInput">
                            關閉
                        </button>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container" v-if="loading" @click="click">
    <div class="row">
        <div class="col-12">
            <button type="button" name="" class="btn btn-primary mx-2" data-toggle="modal" data-target="#InputForExcel">
                匯入系辦資料
            </button>
            <button type="button" name="" class="btn btn-success mx-2" data-target="#TypingInput" data-toggle="modal">
                新增系辦資料
            </button>
            <button class="btn btn-primary mx-2 float-right"
                onclick="location.href='{{route('exportONewdata')}}'">匯出系辦</button>
        </div>
        <table class="table mt-2">
            <thead>
                <tr>
                    <th>編號</th>
                    <th>帳號</th>
                    <th>姓名</th>
                    <th>系所</th>
                    <th>修改</th>
                    <th>刪除</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(item,index) in all">
                    <td scope="row">@{{ index+1 }}</td>
                    <td>@{{ item.Faccount }}</td>
                    <td>@{{ item.Fname }}</td>
                    <td>@{{ item.department }}</td>
                    <td>
                        <button type="button" name="" class="btn btn-info" @click="changedata(index)">
                            修改
                        </button>
                    </td>
                    <td>
                        <form action="{{route('flight.destroy',0)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            @csrf
                            <input type="hidden" name="id" v-model="item.id">
                            <button onclick="return confirm('確認刪除此筆資料?');" type="submit" name="" class="btn btn-danger">
                                刪除
                            </button>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div v-else class="container" style="height:80vh">
    <div class="row">
        <div class="col-12 pt-5">
            <div class="bouncing-loader ">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
@parent

<script>
    new Vue({
        el: "#app",
        data() {
            return {
                loading: false,
                type: {
                    Fname: "",
                    Faccount: "",
                    department: "",                    
                },
                change: {
                    id: 0,
                    Fname: "",
                    Faccount: "",
                    department: "",
                    
                },
                all: [],
                departments: [],
                addname: "",
                upname: "",
                key:""
            };
        },
        methods: {
        async click(){
            const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },
            changedata(index) {
                this.change.id = this.all[index].id;
                this.change.name = this.all[index].Fname;
                this.change.account = this.all[index].Faccount;
                this.change.department = this.all[index].department;
                $("#changeinput").modal("show");
            },
            onFileChange(e) {
                if (e.target.files.length != 0) {
                    //console.log(e.target.files[0].name);
                    this.addname = e.target.files[0].name;
                }
            },
            onFileChange2(e) {
                if (e.target.files.length != 0) {
                    //console.log(e.target.files[0].name);
                    this.upname = e.target.files[0].name;
                }
            },
            CloseInput() {
                //console.log("close");
                this.type.name = "";
                this.type.account = "";
                this.type.department = "";
                this.type.date = "";
                $("#TypingInput").modal("hide");
            }
        },
        created() {
            axios.get("http://irmaterials.nuu.edu.tw/flight/show").then(res => { //book.test
                // axios.get("http://127.0.0.1:8000/flight/show").then(res => {//book.test
                this.all = res.data;
            });
            axios.get("http://irmaterials.nuu.edu.tw/departments/show").then(res => { //book.test
                this.departments = res.data;
                this.loading = true;
            });            
        }
    });
</script>
@endsection