@section('content')
<section class="pt-5" id="app" @click="click">
  <div class="modal fade" id="InputForExcel" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">匯入</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container">
            <div class="row my-2">
              <form action="{{route('iptOfStudents')}}" method="post" enctype="multipart/form-data" class="d-flex">
                @csrf
                <input type="file" id="ExcelAdd" name="select_file" style="display: none" accept=".xlsx, .xls, .csv"
                  v-on:change="onFileChange($event)" />
                <button type="button" class="btn btn-success" onclick="$('#ExcelAdd').click()">

                  新增
                </button>
                <span class="mx-2 my-auto">新增資料在原本資料後方，不會將原資料刪除<br><small>@{{addname}}</small></span>

                <button type="submit" class="btn btn-success" v-if="addname!=''">送出</button>
              </form>
            </div>
            <div class="row my-2">
              <form action="{{route('uptOfStudents')}}" method="post" enctype="multipart/form-data" class="d-flex">
                @csrf
                <input type="file" id="ExcelUpdate" name="select_file" style="display: none" accept=".xlsx, .xls, .csv"
                  v-on:change="onFileChange2($event)" />
                <button type="button" class="btn btn-primary" onclick="$('#ExcelUpdate').click()">

                  更新
                </button>
                <span class="mx-2 my-auto">取代原本資料，不會保留原有資料<br><small>@{{upname}}</small></span>
                <button type="submit" class="btn btn-primary" onclick="return confirm('請注意取代原本資料，『不會保留』原有資料!!!');"
                  v-if="upname!=''">送出</button>
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer d-flex justify-content-between">
          <a href="/storage/Excel%20example/student.xlsx">範例檔案</a>

          <button type="button" class="btn btn-secondary" data-dismiss="modal">
            關閉
          </button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="changeinput" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">修改學生資料</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('student.update', 0) }}" method="post">
            <input type="hidden" name="_method" value="PUT" />
            <input type="hidden" name="id" v-model="change.id" />
            @csrf
            <div class="container">
              <div class="row">
                <div class="form-group">
                  <label for="">姓名</label>
                  <input type="text" name="Sname" class="form-control" placeholder="" aria-describedby="helpId"
                    v-model="change.Sname" />
                  <small id="helpId" class="text-muted">輸入名稱</small>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="">性別</label>
                  <select class="custom-select" id="inputGroupSelect01" name="gender" required v-model="change.gender">
                    <option>男</option>
                    <option>女</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="">學校</label>
                  <input type="text" name="School" class="form-control" placeholder="" aria-describedby="helpId"
                    v-model="change.graduation" />
                  <small id="helpId" class="text-muted">輸入學校名稱</small>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="">招生名額</label>
                  <input type="text" name="category" class="form-control" placeholder="" aria-describedby="helpId"
                    v-model="change.category" />
                  <small id="helpId" class="text-muted">輸入招生名額</small>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="">系組</label>
                  <input type="hidden" name="olddepartment" v-model="change.department">
                  <select class="custom-select" id="inputGroupSelect01" name="department" required>
                    <template v-for="item in departments">
                      <option :value="item.department">@{{ item.department }}</option>
                    </template>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="">家庭收入</label>
                  <select class="custom-select" name="income">
                    <option>一般生</option>
                    <option>中低收入戶</option>
                    <option>低收入戶</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="">身分別</label>
                  <select class="custom-select" name="identifity">
                    <option>一般考生</option>
                    <option>原住民考生</option>
                    <option>離島考生</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="">學校代碼</label>
                  <input type="text" name="schoolnum" class="form-control" placeholder="" aria-describedby="helpId"
                    v-model="change.schoolnum" />
                  <small id="helpId" class="text-muted">輸入學校代碼</small>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="">畢業年度</label>
                  <input type="text" name="year" class="form-control" placeholder="" aria-describedby="helpId"
                    v-model="change.year" />
                  <small id="helpId" class="text-muted">輸入學校代碼</small>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="">通訊地址</label>
                  <input type="text" name="address" class="form-control" placeholder="" aria-describedby="helpId"
                    v-model="change.address" />
                  <small id="helpId" class="text-muted">輸入通訊地址</small>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="CloseInput">
                關閉
              </button>
              <button type="submit" class="btn btn-primary">儲存</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="container" v-if="loading">
    <div class="row">
      <div class="col-12">
        <div class="row">
          <div class="col-4">
            @if(session()->get('status')!="office")
            <button type="button" name="" class="btn btn-primary mx-2" data-toggle="modal" data-target="#InputForExcel">
              匯入學生資料
            </button>
            @endif
          </div>
          <div class="col-8 d-flex flex-row justify-content-end">
            <input name="" class="form-control mx-1" style="width:300px" v-model="filter" placeholder="搜尋" />
            <button class="btn btn-primary" @click="searchname()">搜尋</button>
            <button class="btn btn-primary mx-2" onclick="location.href='{{route('exportSNewdata')}}'">匯出學生</button>
          </div>
        </div>
      </div>
      <table class="table mt-2">
        <thead>
          <tr>
            <th width="50px">編號</th>
            <th width="80px">應試號碼</th>
            <th width="80px">姓名</th>
            <th width="70px">性別</th>
            <th>系組</th>
            <th width="65px">家庭收入</th>
            <th width="70px">身分別</th>
            <th width="70px">招生名額</th>
            <th width="70px">學校代碼</th>
            <th>學校</th>
            <th>畢業年度</th>
            <th>通訊地址</th>
            @if(session()->get('status')!="office")
            <th>修改</th>
            <th>刪除</th>
            @endif
          </tr>
        </thead>
        <tbody>
          <tr v-for="(item,index) in all">
            <td width="50px">@{{ index + 1 }}</td>
            <td width="80px">@{{ item.Snum }}</td>
            <td width="80px">@{{ item.Sname }}</td>
            <td width="70px">@{{ item.gender }}</td>
            <td>@{{ item.department }}</td>
            <td width="65px">@{{ item.income[0] }}</td>
            <td width="70px">@{{ item.identifity[0] }}</td>
            <td width="70px">@{{ item.category }}</td>
            <td width="70px">@{{ item.schoolnum }}</td>
            <td>@{{ item.graduation }}</td>
            <td>@{{ item.year }}</td>
            <td>@{{ item.address }}</td>
            @if(session()->get('status')!="office")
            <td>
              <button type="button" name="" class="btn btn-info" @click="changedata(index)">
                修改
              </button>
            </td>
            <td>
              <form action="{{ route('student.destroy', 0) }}" method="post">
                <input type="hidden" name="_method" value="DELETE" />
                @csrf
                <input type="hidden" name="id" v-model="item.id" />
                <button type="submit" name="" onclick="return confirm('確認刪除此筆資料?');" class="btn btn-danger">
                  刪除
                </button>
              </form>
            </td>
            @endif
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12 d-flex  justify-content-center">
        <div class="block">
          <el-pagination @current-change="changepage" layout="prev, pager, next" :page-size="50"
            :current-page.sync="current" :total="total">
          </el-pagination>
        </div>
      </div>
    </div>
  </div>

  <div v-if="!loading" class="container" style="height:80vh">
    <div class="row">
      <div class="col-12 pt-5">
        <div class="bouncing-loader ">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection @section('script') @parent

<script>
  new Vue({
    el: "#app",
    data() {
      return {
        loading: false,
        change: {
          id: 0,
          Sname: "",
          income:"",
          identifity:"",
          gender:"",
          department: "",
          category: "",
          schoolnum: "",
          year: "",
          address: "",
          graduation:""
        },
        all: [],
        departments: [],
        total:0,
        now:1,
        addname: "",
        upname: "",
        filter:"",
        current:1,
      };
    },
    methods: {
        async click(){
            const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },
      async changepage(currentpage) {
        window.scrollTo(0,0);
        this.loading = false;
        axios.get("http://irmaterials.nuu.edu.tw/student/show?page="+currentpage+"&search="+this.filter
        ).then(res => {
            this.all=res.data.student;
            this.current=parseInt(res.data.current,10);
            this.loading=true
        });
        
      },
      searchname(){
        window.scrollTo(0,0);
        this.loading = false;
        axios.get("http://irmaterials.nuu.edu.tw/student/show?search="+this.filter).then(res => {
          //axios.get("http://127.0.0.1:8000/student/show").then(res => {
          this.all = res.data.student;
          //console.log(res.data);
          this.total=res.data.total;
          this.loading=true
        });
      },
      changedata(index) {
        this.change.id = this.all[index].id;
        this.change.Sname = this.all[index].Sname;
        this.change.Snum = this.all[index].Snum;
        this.change.income = this.all[index].income;
        this.change.identifity = this.all[index].identifity;
        this.change.department = this.all[index].department;
        this.change.graduation = this.all[index].graduation;
        this.change.gender = this.all[index].gender;
        this.change.category = this.all[index].category;
        this.change.schoolnum = this.all[index].schoolnum;
        this.change.year = this.all[index].year;
        this.change.address = this.all[index].address;
        $("#changeinput").modal("show");
      },
      onFileChange(e) {
        if (e.target.files.length != 0) {
          //console.log(e.target.files[0].name);
          this.addname = e.target.files[0].name;
        }
      },
      onFileChange2(e) {
        if (e.target.files.length != 0) {
          //console.log(e.target.files[0].name);
          this.upname = e.target.files[0].name;
        }
      },
      CloseInput() {
        //console.log("close");
        this.type.Sname = "";
        this.type.Snum = "";
        this.type.department = "";
        $("#TypingInput").modal("hide");
      }
    },
    async created() {
      this.loading = false;
      const res = await axios.get("http://irmaterials.nuu.edu.tw/student/show");
      this.all=res.data.student;
      this.total=res.data.total;
      axios.get("http://irmaterials.nuu.edu.tw/departments/show").then(res => { //book.test
          this.departments = res.data;
          this.loading = true;
      });
    }
  });
</script>
@endsection