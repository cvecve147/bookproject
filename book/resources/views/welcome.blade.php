<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
        @if (Session::get('error'))
            <div class="alert alert-success" id="app">
                {{ Session::get('error')}}
            </div>
        @endif
        <form name='redirect' action='http://sso.nuu.edu.tw/preLogin.php' method='POST'>
            <input type='hidden' name='system_name' value='書審輔助系統'>
            <input type='submit' value='submit' style="display:none">
        </form>
        <script type='text/javascript'>
            document.redirect.submit();
        </script>
    </body>
</html>
