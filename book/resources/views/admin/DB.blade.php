@extends('admin.allHeaders')
@section('content')


<div id="app" class="pt-5 " @click="click">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <select class="custom-select" v-model="selected" @change="getNewTable">
                    <option value="1">flights </option>
                    <option value="2">teachers</option>
                    <option value="3">students&department</option>
                    <option value="4">matches</option>
                    <option value="5">match_nums</option>
                    <option value="6">mark_data</option>
                    <option value="7">grades</option>
                    <option value="8">sessions</option>
                </select>
            </div>
            <div class="col-12 my-3">

            </div>
            <div class="col-12" v-if="!loading">
                <el-table :data="data" style="width: 100%" stripe>
                    <div v-if="selected==1">
                        <el-table-column type="index" width="80" label="編號">
                        </el-table-column>
                        <el-table-column prop="Faccount" label="帳號">
                        </el-table-column>
                        <el-table-column prop="Fname" label="姓名">
                        </el-table-column>
                        <el-table-column prop="position" label="職位">
                        </el-table-column>
                        <el-table-column prop="department" label="系組">
                        </el-table-column>
                        <el-table-column prop="login" label="權限">
                        </el-table-column>
                    </div>
                    <div v-if="selected==2">
                        <el-table-column type="index" width="80" label="編號">
                        </el-table-column>
                        <el-table-column prop="Taccount" label="帳號">
                        </el-table-column>
                        <el-table-column prop="Tname" label="姓名">
                        </el-table-column>
                        <el-table-column prop="department" label="系組">
                        </el-table-column>
                        <el-table-column prop="login" label="權限">
                        </el-table-column>
                    </div>
                    <div v-if="selected==3">
                        <el-table-column key="0" type="index" width="80" label="編號">
                        </el-table-column>
                        <el-table-column key="1" prop="Snum" label="應試號碼">
                        </el-table-column>
                        <el-table-column key="2" prop="Sname" width="70" label="姓名">
                        </el-table-column>
                        <el-table-column key="3" prop="category" width="70" label="名額">
                        </el-table-column>
                        <el-table-column key="4" prop="gender" width="70" label="性別">
                        </el-table-column>
                        <el-table-column key="5" prop="graduation" width="70" label="畢業學校">
                        </el-table-column>
                        <el-table-column key="6" prop="department" label="系組">
                        </el-table-column>
                        <el-table-column key="7" prop="departmentid" label="部門ID">
                        </el-table-column>
                        <el-table-column key="7" prop="income" width="70" label="收入">
                        </el-table-column>
                        <el-table-column key="8" prop="identifity" width="80" label="身分別">
                        </el-table-column>
                        <el-table-column key="9" prop="login" width="72" label="權限">
                        </el-table-column>
                        <el-table-column key="10" prop="address" label="地址">
                        </el-table-column>
                        <el-table-column key="11" prop="year" label="年度">
                        </el-table-column>
                        <el-table-column key="12" prop="schoolnum" label="學校年度">
                        </el-table-column>
                    </div>
                    <div v-if="selected==4">
                        <el-table-column type="index" width="80" label="編號">
                        </el-table-column>
                        <el-table-column prop="Taccount" label="姓名">
                        </el-table-column>
                        <el-table-column prop="Snum" label="應試號碼">
                        </el-table-column>
                        <el-table-column prop="department" label="系組">
                        </el-table-column>
                        <el-table-column prop="choose" label="選擇">
                        </el-table-column>
                        <el-table-column prop="login" label="權限">
                        </el-table-column>
                    </div>
                    <div v-if="selected==5">
                        <el-table-column type="index" width="80" label="編號">
                        </el-table-column>
                        <el-table-column prop="number" label="數目">
                        </el-table-column>
                        <el-table-column prop="department" label="系組代碼">
                        </el-table-column>
                        <el-table-column prop="login" label="權限">
                        </el-table-column>
                    </div>
                    <div v-if="selected==6">
                        <el-table-column type="index" width="80" label="編號">
                        </el-table-column>
                        <el-table-column prop="project" label="管道">
                        </el-table-column>
                        <el-table-column prop="Bitem" label="大項目">
                        </el-table-column>
                        <el-table-column prop="Mitem" label="中項目">
                        </el-table-column>
                        <el-table-column prop="Sitem" label="小項目">
                        </el-table-column>
                        <el-table-column prop="title" label="標題">
                        </el-table-column>
                        <el-table-column prop="HighScore" label="最高分">
                        </el-table-column>
                        <el-table-column prop="lowScore" label="最低分">
                        </el-table-column>
                        <el-table-column prop="percent" label="百分比">
                        </el-table-column>
                        <el-table-column prop="department" label="系組">
                        </el-table-column>
                        <el-table-column prop="login" label="權限">
                        </el-table-column>
                    </div>
                    <div v-if="selected==7">
                        <el-table-column type="index" width="80" label="編號">
                        </el-table-column>
                        <el-table-column prop="Snum" label="應試號碼">
                        </el-table-column>
                        <el-table-column prop="Bitem" label="大項目">
                        </el-table-column>
                        <el-table-column prop="Mitem" label="中項目">
                        </el-table-column>
                        <el-table-column prop="Taccount" label="老師帳號">
                        </el-table-column>
                        <el-table-column prop="department" label="系組">
                        </el-table-column>
                        <el-table-column prop="score" label="成績">
                        </el-table-column>
                        <el-table-column prop="block" label="區間">
                        </el-table-column>
                        <el-table-column prop="login" label="權限">
                        </el-table-column>
                    </div>
                    <div v-if="selected==8">
                        <el-table-column type="index" width="80" label="編號">
                        </el-table-column>
                        <el-table-column prop="user_id" label="user_id">
                        </el-table-column>
                        <el-table-column prop="ip_address" label="ip_address">
                        </el-table-column>
                        <el-table-column prop="user_agent" label="user_agent">
                        </el-table-column>
                        <el-table-column prop="department" label="department">
                        </el-table-column>
                        <el-table-column prop="payload" label="payload">
                        </el-table-column>
                        <el-table-column prop="last_activity" label="last_activity">
                        </el-table-column>
                    </div>
                </el-table>

            </div>
            <div class="col-12 d-flex  justify-content-center" v-if="selected==3">
                <div class="block">
                    <el-pagination @current-change="changepage" layout="prev, pager, next" :page-size="50"
                        :total="total">
                    </el-pagination>
                </div>
            </div>
            <div v-if="loading" class="container" style="height:80vh">
                <div class="row">
                    <div class="col-12 pt-5">
                        <div class="bouncing-loader ">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection
@section('script')
@parent
<script>
    new Vue({
    el: "#app",
    
    data() {
        return {
            selected:1,
            data:[],
            loading:false,
            total:10
        };
    },
    methods: {
        async click(){
            const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },
        changepage(val){
            window.scrollTo(0,0);
            val-=1
            axios.get("http://irmaterials.nuu.edu.tw/admin/show?DB="+this.selected+"&page="+val
            ).then(res => {
                this.data=res.data
                this.loading=false
            });
        },
        getNewTable() {
            this.loading=true
            axios.get("http://irmaterials.nuu.edu.tw/admin/show?DB="+this.selected
            ).then(res => {
                //console.log(res.data)
                this.data=res.data
                if(this.selected==3){
                    axios.get("http://irmaterials.nuu.edu.tw/admin/show?gettotal="+this.selected
                    ).then(res => {
                        this.total=res.data
                    })
                }
                this.loading=false
            });
        }
    },
    created() {
        this.loading=true
        axios.get("http://irmaterials.nuu.edu.tw/admin/show?DB="+this.selected
        ).then(res => {
            //console.log(res.data)
            this.data=res.data            
            this.loading=false
        });
    }

    });
</script>
@endsection