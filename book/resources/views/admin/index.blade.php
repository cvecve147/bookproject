@extends('admin.allHeaders')
@section('content')

<div id="app" class="pt-5" @click="click"> 
    <div class="container" v-if="!loading">
        <div class="row">
            <div class="col-6">
                <label for="exampleFormControlSelect1">Search</label>
                <input name="" class="form-control" v-model="filter" />
            </div>
            <div class="col-6">
                <label for="exampleFormControlSelect1">Department</label>
                <select class="form-control" v-model="Department">
                    <option value="">全部</option>
                    <template v-for="item in departments">
                        <option :value="item.department">@{{ item.department}}</option>
                    </template>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Faccount</label>
            <input type="text" name="Faccount" class="form-control" v-model="Faccount" required />
            <div>
                <template v-for="(item,index) in filers"><button class="btn btn-primary m-2"
                        @click="getitem(item.Faccount,item.department,item.login,item.Fname)">
                        @{{ item.Faccount }} @{{ item.department }} @{{ item.login }}
                    </button>
                </template>
            </div>
        </div>
        <form action="{{ route('admin.store', 0) }}" method="post">
            @csrf
            <div style="display: none;">
                <input type="text" name="Faccount" class="form-control" v-model="Faccount" required />
                <input type="text" name="department" class="form-control" v-model="ac_department" required />
                <input type="text" name="login" class="form-control" v-model="login" required />
                <input type="text" name="Fname" class="form-control" v-model="Fname" required />
            </div>
            <button class="btn btn-primary btn-block">
                送出
            </button>
        </form>
    </div>
    <div v-else class="container" style="height:80vh">
        <div class="row">
            <div class="col-12 pt-5">
                <div class="bouncing-loader ">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@parent
<script>
    new Vue({
    el: "#app",
    computed: {
        filers() {
            var objs = this;
            return this.AllFaccount.filter(patient => {
                find = false;
                if (
                (patient["Faccount"].toLowerCase().indexOf(objs.filter.toLowerCase()) !=
                    -1 ||
                this.filter == "") && (patient["department"].toLowerCase().indexOf(objs.Department.toLowerCase()) !=
                    -1 ||this.department == "")
                ) {
                    find = true;
                }
                return find;
            });
        }
    },
    data() {
        return {
            filter: "",
            Faccount: "",
            ac_department: "",
            login: "",
            Fname: "",
            AllFaccount: [],
            Alllogin: [],
            Department:'',
            Departmens:[],
            loading:false,
        };
    },
    methods: {
        async click(){
            const res=await axios.get("http://irmaterials.nuu.edu.tw/TimeRequestReset")
        },
        getitem(Faccount,department,login,Fname) {
            this.Faccount = Faccount;
            this.ac_department = department;
            this.login = login;
            this.Fname = Fname;
        }
    },
    created() {
        this.loading = true;
        axios.get("http://irmaterials.nuu.edu.tw/admin/show"
        ).then(res => {
            //console.log(res.data);
            this.AllFaccount = res.data;
        });
        axios.get("http://irmaterials.nuu.edu.tw/departments/show").then(res => { //book.test
            this.departments = res.data;
            //console.log(this.departments);
            
            this.loading = false;
        });
    }

    });
</script>
@endsection